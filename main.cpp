#include <iostream>
#include <cstdlib>
#include <cstring>

#include "Driver.h"

int main( const int argc, const char **argv ) {
   /** check for the right # of arguments **/
   if( argc == 2 ) {
      CTM::Driver driver;
     
      /** simple help menu **/
      if( std::strncmp( argv[ 1 ], "-h", 2 ) == 0 )
      {
         std::cout << "use -o for pipe to std::cin\n";
         std::cout << "just give a filename to count from a file\n";
         std::cout << "use -h to get this menu\n";
         return( EXIT_SUCCESS );
      }
      /** Start reading input file with code **/
      else
      {
         driver.parse( argv[1] );
      }
   }
   else {
      /** exit with failure condition **/
      return ( EXIT_FAILURE );
   }
   return( EXIT_SUCCESS );
}
