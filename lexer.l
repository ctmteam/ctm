O   [0-7]
D   [0-9]
NZ  [1-9]
L   [a-zA-Z_]
A   [a-zA-Z_0-9]
H   [a-fA-F0-9]
HP  (0[xX])
E   ([Ee][+-]?{D}+)
P   ([Pp][+-]?{D}+)
FS  (f|F|l|L)
IS  (((u|U)(l|L|ll|LL)?)|((l|L|ll|LL)(u|U)?))
CP  (u|U|L)
SP  (u8|u|U|L)
ES  (\\(['"\?\\abfnrtv]|[0-7]{1,3}|x[a-fA-F0-9]+))
WS  [ \t\v\n\f]

%{
/* C++ string header, for string ops below */
#include <string>
#include <iostream>

/* Implementation of yyFlexScanner */ 
#include "Scanner.h"
#undef  YY_DECL
#define YY_DECL int CTM::Scanner::yylex( CTM::Parser::semantic_type * const lval, CTM::Parser::location_type *loc )

/* typedef to make the returns for the tokens shorter */
using token = CTM::Parser::token;

/* msvc2010 requires that we exclude this header file. */
#define YY_NO_UNISTD_H

/* update location on matching */
#define YY_USER_ACTION loc->step(); loc->columns(yyleng);

%}

%option debug
%option warn nodefault
%option yyclass="CTM::Scanner"
%option noyywrap
%option c++

%%
%{          /** Code executed at the beginning of yylex **/
            yylval = lval;
%}


[/][*][^*]*[*]+([^*/][^*]*[*]+)*[/]  { /* skip comments */ }
"//"[^\r\n]*	{ /* skip comments */ }

"print"		{ return( token::PRINT); }
"int"		{ return( token::INT); }
"float"		{ return( token::FLOAT); }
"bool"		{ return( token::BOOL); } 
"break"		{ return( token::BREAK); }
"const"		{ return( token::CONST); }
"continue" 	{ return( token::CONTINUE); }
"else"		{ return( token::ELSE); }
"if"		{ return( token::IF); }
"return"	{ return( token::RETURN); }
"void"		{ return( token::VOID); }
"while"		{ return( token::WHILE); }
"do"		{ return( token::DO); }

{HP}{H}+{IS}?				{ yylval->build< std::string >( yytext ); return( token::I_CONSTANT); }
{NZ}{D}*{IS}?				{ yylval->build< std::string >( yytext ); return( token::I_CONSTANT); }
"0"{O}*{IS}?				{ yylval->build< std::string >( yytext ); return( token::I_CONSTANT); }
{CP}?"'"([^'\\\n]|{ES})+"'"		{ yylval->build< std::string >( yytext ); return( token::I_CONSTANT); }

{D}+{E}{FS}?				{ yylval->build< std::string >( yytext ); return( token::F_CONSTANT); }
{D}*"."{D}+{E}?{FS}?			{ yylval->build< std::string >( yytext ); return( token::F_CONSTANT); }
{D}+"."{E}?{FS}?			{ yylval->build< std::string >( yytext ); return( token::F_CONSTANT); }
{HP}{H}+{P}{FS}?			{ yylval->build< std::string >( yytext ); return( token::F_CONSTANT); }
{HP}{H}*"."{H}+{P}{FS}?			{ yylval->build< std::string >( yytext ); return( token::F_CONSTANT); }
{HP}{H}+"."{P}{FS}?			{ yylval->build< std::string >( yytext ); return( token:: F_CONSTANT); }


"true"		{ yylval->build< std::string >( yytext ); return( token::B_CONSTANT); }
"false"		{ yylval->build< std::string >( yytext ); return( token::B_CONSTANT); }

{L}({L}|{D})*				{ yylval->build< std::string >( yytext ); return( token::IDENTIFIER); }

({SP}?\"([^"\\\n]|{ES})*\"{WS}*)+	{  yylval->build< std::string >( yytext ); return( token::STRING_LITERAL); }



"="		{  		  
		  yylval->build< std::string >( yytext );
               	  return( token::EQ); 
		}
"++"		{ 
		  yylval->build< std::string >( yytext );
		  return( token::INC_OP); 
		}
"--"		{ 
		  yylval->build< std::string >( yytext );
	 	  return( token::DEC_OP); 
		}
"+"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::INC ); 
		}
"-"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::DEC ); 
		}
"*"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::MULTIPLY ); 
		}
"/"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::DIVIDE ); 
		}
"%"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::MODULO ); 
		}
"&&"		{ 	
		  yylval->build< std::string >( yytext );
		  return( token::AND_OP); 
		}
"||"		{
		  yylval->build< std::string >( yytext ); 
		  return( token::OR_OP); 
		}
"!"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::EXCLAMATION ); 
		}

"=="		{ 
		  yylval->build< std::string >( yytext );
		  return( token::EQ_OP); 
		}
"!="		{ 
		  yylval->build< std::string >( yytext );
		  return( token::NE_OP); 
		}
"<"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::LESS ); 
		}
">"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::GREATER ); 
		}
"<="		{  
		  yylval->build< std::string >( yytext );
               	  return( token::LE_OP ); 
		}
">="		{  
		  yylval->build< std::string >( yytext );
               	  return( token::GE_OP ); 
		}
"~"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::TILDE ); 
		}
"&"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::AND ); 
		}
"^"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::CARET ); 
		}
"|"		{  
		  yylval->build< std::string >( yytext );
               	  return( token::OR ); 
		}
"^^"		{ 
		  yylval->build< std::string >( yytext );
		  return( token::XOR_OP); 
		}
"{"	{  		  
               	  return( token::CURLY_OPEN ); 
		}
"}"	{  
               	  return( token::CURLY_CLOSE ); 
		}
"("		{  
               	  return( token::ROUND_OPEN ); 
		}
")"		{  
               	  return( token::ROUND_CLOSE ); 
		}
"["	{  
               	  return( token::BRACKET_OPEN ); 
		}
"]"	{  
               	  return( token::BRACKET_CLOSE ); 
		}
","		{  
		  yylval->build< std::string >( yytext );
               	  return( token::COMMA ); 
		}
";"		{  
               	  return( token::SEMICOLON ); 
		}


"\n" 		{
		  loc->lines(); 
		}

{WS}+		{}
.		{}

%%

int yywrap(void)        
{
    return 1;           
}