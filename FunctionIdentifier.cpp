#include "FunctionIdentifier.h"

namespace CTM {

FunctionIdentifier::FunctionIdentifier() {}

FunctionIdentifier::FunctionIdentifier(EType returnType, std::string &name, std::vector<VariableIdentifier> &parameters, bool isDefined = false) :
	_name(name),
	_parameters(parameters),
	_returnType(returnType),
	_defined(isDefined)
{
	_ordinaryName = FunctionIdentifier::getOrdinaryName(*this);
}

std::string FunctionIdentifier::name(){
  return _name;
}

std::string FunctionIdentifier::name() const{
  return _name;
}

FunctionIdentifier& FunctionIdentifier::operator=(const FunctionIdentifier& copyFrom){
  _name = copyFrom._name;
  _parameters = copyFrom._parameters;
  _returnType = copyFrom._returnType;
  _ordinaryName = copyFrom._ordinaryName;
  _defined = copyFrom._defined;
}


std::string FunctionIdentifier::ordinaryName() {
  return _ordinaryName;
}

std::string FunctionIdentifier::ordinaryName() const{
  return _ordinaryName;
}

bool FunctionIdentifier::defined() {
	return _defined;
}

bool FunctionIdentifier::defined() const {
	return _defined;
}

void FunctionIdentifier::setDefined() {
	_defined = true;
}

std::vector<VariableIdentifier> &FunctionIdentifier::parameters() {
	return _parameters;
}

EType FunctionIdentifier::returnType() {
	return _returnType;
}

std::string FunctionIdentifier::getOrdinaryName(FunctionIdentifier &identifier) {
	std::stringstream ss;
	ss << "_" << identifier._name; 
	for(VariableIdentifier el : identifier._parameters) {
		EType tmp = el.type();
		ss << '_' << toString(tmp);
	}
	return ss.str();
}

std::string FunctionIdentifier::getOrdinaryName(std::string &functionName, std::vector<EType> &parameters) {
	std::stringstream ss;
	ss << "_" << functionName; 
	for(EType el : parameters) {
		ss << '_' << toString(el);
	}
	return ss.str();
}

}
