#include "EExp.h"

namespace CTM {

std::string toString(EExp &type) {
	switch(type) {
		case EExp::VAR:
			return "var";
		case EExp::BOOL:
			return "bool";
		case EExp::INT:
			return "int";
		case EExp::FLOAT:
			return "float";
		case EExp::VOID:
			return "void";
		case EExp::FUNC:
			return "function";
		default:
			return "";
	}
}

int getSize(EExp &type) {
	switch(type) {
		case EExp::BOOL:
			return 1;
		case EExp::INT:
			return 4;
		case EExp::FLOAT:
			return 4;
		default:
			return 0;
	}
}

EExp convert(EType &type) {
	switch(type) {
		case EType::BOOL:
			return EExp::BOOL;
		case EType::INT:
			return EExp::INT;
		case EType::FLOAT:
			return EExp::FLOAT;
		default:
			return EExp::VOID;
	}
}

}
