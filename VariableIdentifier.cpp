#include "VariableIdentifier.h"

CTM::VariableIdentifier::VariableIdentifier() : 
	_type(EType::VOID),
	_identifier(""),
	_isConstant(false),_positionStack(-1),_isGlobal(false)
{}

CTM::VariableIdentifier::VariableIdentifier(std::string identifier = "", EType type = EType::VOID, bool isConst = false, int positionStack = -1, bool isGlobal = false) : 
	_type(type),
	_identifier(identifier),
	_isConstant(isConst),
	_positionStack(positionStack),
	_isGlobal(isGlobal)
{}

CTM::VariableIdentifier::VariableIdentifier(const CTM::VariableIdentifier &copyFrom) :
	_type(copyFrom._type),
	_identifier(copyFrom._identifier),
	_isConstant(copyFrom._isConstant),
	_positionStack(copyFrom._positionStack),
	_sizeArray(copyFrom._sizeArray),
	_haveData(copyFrom._haveData),
	_isGlobal(copyFrom._isGlobal)
{

}

std::string CTM::VariableIdentifier::identifier() {
	return _identifier;
}

std::string CTM::VariableIdentifier::identifier() const{
	return _identifier;
}

CTM::EType CTM::VariableIdentifier::type() {
	return _type;
}

bool CTM::VariableIdentifier::isConstant() {
	return _isConstant;
}	

void CTM::VariableIdentifier::setIdentifier(std::string ident){
	_identifier = ident;
}

int CTM::VariableIdentifier::sizeArray(){
	return _sizeArray;
}
void CTM::VariableIdentifier::setSizeArray(int size){
	_sizeArray = size;
}
void CTM::VariableIdentifier::clearValueStack(){
	valueStack.clear();
	
}
void CTM::VariableIdentifier::pushStackValues(std::pair<std::string, CTM::EExp> value){
	valueStack.push_back(value);
}

int CTM::VariableIdentifier::positionStack(){
	return _positionStack;
}
void CTM::VariableIdentifier::setPositionStack(int positionStack){
	_positionStack = positionStack;
}
bool CTM::VariableIdentifier::haveData(){
	return _haveData;
}
void CTM::VariableIdentifier::setHaveData(bool have){
	_haveData = have; 
}

std::pair<std::string, CTM::EExp> CTM::VariableIdentifier::popStackValues(int index){
	return valueStack[index];
}

std::pair<std::string, CTM::EExp> CTM::VariableIdentifier::popBackStackValues(){
	std::pair<std::string, CTM::EExp> returnVal = valueStack.back();
	/*valueStack.pop_back();
	*/
	for(std::pair<std::string, CTM::EExp> a : valueStack) {
		std::cout << a.first << " + " << toString(a.second) << std::endl;
	}
	return returnVal;
}

int CTM::VariableIdentifier::getStackSize(){
	return valueStack.size();
}

bool CTM::VariableIdentifier::isGlobal(){
	return _isGlobal;
}
bool CTM::VariableIdentifier::setIsGlobal(bool global){
	_isGlobal = global;
}


CTM::VariableIdentifier CTM::VariableIdentifier::fromVectorFactory(std::string &name, std::vector<EType> &types, int positionStack,bool isGlobal) {
	EType type, aktualniType;
	bool isConstant = false;
	for (EType aktualniType : types) {
		if(aktualniType == EType::CONST) {
			isConstant = true;
		}
		else {
			type = aktualniType;
			if(isConstant == true) {
				break;
			}				
		}								
	}
	return VariableIdentifier(name, type, isConstant, positionStack,isGlobal); 
}

CTM::VariableIdentifier& CTM::VariableIdentifier::operator=(const VariableIdentifier &copyFrom) {
	_type = copyFrom._type;
	_identifier = copyFrom._identifier;
	_isConstant = copyFrom._isConstant;
	_positionStack = copyFrom._positionStack;
	_sizeArray = copyFrom._sizeArray;
	_haveData = copyFrom._haveData;
	_isGlobal = copyFrom._isGlobal;
	return (*this);
}

CTM::VariableIdentifier& CTM::VariableIdentifier::operator=(VariableIdentifier &&moveFrom) {
	_type = moveFrom._type;
	_identifier = moveFrom._identifier;
	_isConstant = moveFrom._isConstant;
	_positionStack = moveFrom._positionStack;
	_sizeArray = moveFrom._sizeArray;
	_haveData = moveFrom._haveData;
	_isGlobal = moveFrom._isGlobal;
	return (*this);
}
int CTM::VariableIdentifier::getSize(){
	if((_type == EType::INT) || (_type == EType::FLOAT)){
		return 4;
	}else if(_type == EType::BOOL){
		return 4;

	}
}
