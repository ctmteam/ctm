#ifndef __E_TYPES_H__
#define __E_TYPES_H__


#include <string>

namespace CTM{
/**
* Enum for enabled types
**/
enum class EType  {
	VOID,
	BOOL,
	INT,
	FLOAT,
	CONST
};

std::string toString(EType &);
}

#endif /* END __E_TYPES_H__ */

