#ifndef __MCSCANNER_HPP__
#define __MCSCANNER_HPP__ 1

#if ! defined(yyFlexLexerOnce)
#include <FlexLexer.h>
#endif

#include "parser.tab.hpp"
#include "location.hh"

namespace CTM{
/**
 * Using Flex for lexical analyzation of input code
**/
class Scanner : public yyFlexLexer{
public:
   
   Scanner(std::istream *in) : yyFlexLexer(in)
   {
      loc = new CTM::Parser::location_type(); /* actual processed line */
   };
   virtual ~Scanner() {
      delete loc;
   };

   //get rid of override virtual function warning
   using FlexLexer::yylex;

   virtual
   int yylex( CTM::Parser::semantic_type * const lval, 
              CTM::Parser::location_type *location );
   // YY_DECL defined in mc_lexer.l
   // Method body created by flex in mc_lexer.yy.cc

private:
   /* yyval ptr */
   CTM::Parser::semantic_type *yylval = nullptr;
   /* location ptr */
   CTM::Parser::location_type *loc    = nullptr;
};

} /* end namespace MC */

#endif /* END __MCSCANNER_HPP__ */
