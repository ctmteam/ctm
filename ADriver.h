#ifndef __ADRIVER_H__
#define __ADRIVER_H__

#include <fstream>
#include <string>
#include <istream>
#include <sstream>
#include <memory>
#include <iostream>
#include <stack>
#include <regex>
#include <cstdio>
#include <cstdlib>

#include "Scanner.h"
#include "parser.tab.hpp"
#include "EType.h"
#include "EExp.h"
#include "VariableIdentifier.h"
#include "FunctionIdentifier.h"

namespace CTM{

class ADriver {
public:
   ADriver() = default;
   virtual ~ADriver();

   virtual void pushIdentifier(std::string) = 0;
   virtual void pushType(EType) = 0;


   virtual VariableIdentifier findVariable(std::string var) = 0;
   virtual FunctionIdentifier findFunctionByOrdinary(std::string &) = 0;


   virtual void setIsFunctionDeclaration(bool) = 0;
   virtual void setConstant(std::pair<std::string, CTM::EExp>) = 0;

   virtual bool haveDefinedFunctionIdentifier(const FunctionIdentifier &) = 0;
   virtual void pushFunctionIdentifier(const FunctionIdentifier &) = 0;
   virtual void pushVariableIdentifier(std::string &name, std::vector<EType> &types, bool) = 0;
   virtual void pushVariableIdentifier(std::string &name, std::vector<EType> &types, bool, bool) = 0;

   virtual std::pair<std::string, EExp> castOp(CTM::EType type, std::pair<std::string, CTM::EExp> operator2) = 0;
   virtual std::pair<std::string, EExp> addOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> subOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> mulOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> divOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> andOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> orOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> eqOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> nonEqOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2) = 0;
   virtual std::pair<std::string, EExp> exclamationOP(std::pair<std::string, CTM::EExp> value) = 0;
   virtual void setLastArraySize(std::string) = 0;

   /* methods generating labels for if - else if - else */
   virtual void startLabelNext() = 0;
   virtual void labelNext() = 0;
   virtual void startLabelEnd() = 0;
   virtual void labelEnd(bool elseif) = 0;

   /* methods generating labels for while loop */
   virtual void startLabelWhile() = 0;
   virtual void labelWhile() = 0;
   virtual void whileExpression() = 0;
   virtual void startWhileEnd() = 0;
   virtual void whileBreak() = 0;
   virtual void checkIfWhile() = 0;

   /* return from function */
   virtual void jumpOutOfFunction() = 0;
   virtual void jumpOutOfFunction(std::pair<std::string, EExp> &) = 0;

   virtual void callFunction(std::string &) = 0;
   virtual void callFunction(std::string &, std::vector<std::pair<std::string, CTM::EExp>> &) = 0;

   virtual void setLastIdent(std::string lastIdent) = 0;
   virtual void setLastType(CTM::EType lastType) = 0;
   virtual void setLastIndex(std::string index) = 0;
   virtual void assigmentExppresion() = 0;

   virtual void printString(std::string value) = 0;
   virtual void printValue(std::pair<std::string, CTM::EExp> value) = 0;

   virtual void setVarSpace(std::string &) = 0;
   virtual void clearVarSpace() = 0;

protected:
   std::unique_ptr<CTM::Parser> parser; /* parser for syntax analyzation */
   std::unique_ptr<CTM::Scanner> scanner; /* scanner for lexical analyzation */
   std::ofstream outputFile; /* Outputfile with .asm code */
   
   std::FILE* outDataSegmentFile;	
   std::FILE* outCodeSegmentFile;
   std::FILE* outDataStringSegmentFile;
   void parseStream( std::istream &stream );
   virtual void printFileHeader() = 0; /* Header for .asm file */
   virtual void saveOutPutFile() = 0;

private:
   /* Message's text color */
   const std::string RED_COLOR = "\033[1;31m"; /* error message */
   const std::string GREEN_COLOR = "\033[1;32m"; /* success message */
   const std::string DEFAULT_COLOR = "\033[0m"; /* normal message */

   virtual void getFromStack(std::string reg, CTM::EType type) = 0;
   virtual void setToStack(std::string reg, CTM::EType type) = 0;
   virtual std::string deterPartResult(std::string value, std::string reg, CTM::EExp type , std::string otherValue, CTM::EExp otherType) = 0;

   /* Integer arithmetic MIPS instruction */
   virtual void addInt(std::string d,std::string s,std::string t) = 0;
   virtual void addiInt(std::string t,std::string s,int C)=0; 
   virtual void subInt(std::string d,std::string s,std::string t) = 0;
   virtual void multInt(std::string d, std::string s,std::string t) = 0;
   virtual void divInt(std::string s,std::string t) = 0;
  
   /* Float arithmetic MIPS instruction */
   virtual void addFloat(std::string x,std::string y,std::string z) = 0;
   virtual void subFloat(std::string x,std::string y,std::string z) = 0;
   virtual void mulFloat(std::string x,std::string y,std::string z) = 0;
   virtual void divFloat(std::string x,std::string y,std::string z) = 0;
   
   /* Logical MIPS instruction */
   virtual void andL(std::string d,std::string s,std::string t) = 0;
   virtual void andiL(std::string t,std::string s,int C)=0;
   virtual void orL(std::string d,std::string s,std::string t) = 0;
   virtual void oriL(std::string t,std::string s,int C)=0;
   virtual void xorL(std::string d,std::string s,std::string t) = 0;
   virtual void xoriL(std::string t,std::string s,int C)=0;
   virtual void nor(std::string d,std::string s,std::string t) = 0;
   virtual void slt(std::string d,std::string s,std::string t) = 0;
   virtual void slti(std::string t,std::string s,int C)=0;
   
   /* Bitwise Shift MIPS instruction */
   virtual void sll(std::string d,std::string t,int shamt) = 0;
   virtual void srl(std::string d,std::string t,int shamt) = 0;
   virtual void sllv(std::string d,std::string t,std::string s) = 0;
   virtual void srlv(std::string d,std::string t,std::string s) = 0;
  
   /* Data Transfer MIPS instruction */
   virtual void moveHi(std::string d) = 0;
   virtual void moveLo(std::string d) = 0;
   virtual void li(std::string r,std::string s) = 0;
   virtual void liFloat(std::string r,std::string s) = 0;

   /* Pseudo MIPS instruction */
   virtual void move(std::string r,std::string s) = 0;
   virtual void clear(std::string rt) = 0;
   virtual void notP(std::string rt,std::string rs) = 0;
  
   /* Branch MIPS instruction */
   virtual void beq(std::string s,std::string t,std::string loop) = 0;
   virtual void bne(std::string s,std::string t,std::string loop) = 0;
   virtual void bc1t(std::string loop) = 0;
   virtual void bc1f(std::string loop) = 0;
   virtual void c_eq_s(std::string s, std::string d) = 0;

   /* Jump MIPS instruction */
   virtual void jr(std::string s) = 0;
   virtual void jal(std::string &s) = 0;

   /* Syscal MIPS instruction */
   virtual void sysPrintInt(int intValue) = 0;
   virtual void sysPrintFloat(std::string &floatValue) = 0;
   virtual void sysPrintString(std::string stringValue) = 0;
 
   virtual void sysPrintVariableInt(std::string s, int bytes) = 0;
   virtual void sysPrintVariableFloat(std::string s, int bytes) = 0;
   virtual void sysPrintVariableBool(std::string s, int bytes) = 0;

   virtual void sysReadInteger() = 0;
   virtual void sysReadFloat() = 0;

   virtual void mtc1(std::string d, std::string s) = 0;
   virtual void mfc1(std::string s, std::string d) = 0;

   virtual void cvtIntToFloat(std::string d, std::string s) = 0;
   virtual void cvtFloatToInt(std::string d, std::string s) = 0;

   virtual void lwc1(std::string d, std::string s, int bytes) = 0;
   virtual void swc1(std::string d, std::string s, int bytes) = 0;
   virtual void sw(std::string d, std::string s, int bytes) = 0;
   virtual void saveFloat(std::string d, std::string s, int bytes) = 0;
   virtual void sb(std::string d, std::string s, int bytes) = 0;
   virtual void lw(std::string d, std::string s, int bytes) = 0;
   virtual void lb(std::string d, std::string s, int bytes) = 0;
   

};

} /* end namespace CTM */
#endif /* END __ADRIVER_H__ */
