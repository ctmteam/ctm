%skeleton "lalr1.cc"
%require  "3.0"
%debug 
%defines 
%define api.namespace {CTM}
%define parser_class_name {Parser}

%code requires{
   #include "VariableIdentifier.h"
   #include "FunctionIdentifier.h"

   namespace CTM {
      class ADriver;
      class Scanner;
      enum class EType;
      enum class EExp;
   }

  // The following definitions is missing when %locations isn't used
  # ifndef YY_NULLPTR
  #  if defined __cplusplus && 201103L <= __cplusplus
  #   define YY_NULLPTR nullptr
  #  else
  #   define YY_NULLPTR 0
  #  endif
  # endif
}

%parse-param { Scanner  &scanner  }
%parse-param { ADriver  &driver  }

%code{   
   #include <iostream>
   #include <cstdlib>
   #include <fstream>
   #include <vector>
   #include <string>

   /* include for all driver functions */
   #include "EType.h"
   #include "EExp.h"
   #include "ADriver.h"
   #include "VariableIdentifier.h"

#undef yylex
#define yylex scanner.yylex
}

%define api.value.type variant
%define parse.assert

/* definition of all tokens, which could be returned from lexer.l */
%token END
%token <std::string> IDENTIFIER 
%token <std::string> I_CONSTANT <std::string> F_CONSTANT <std::string> B_CONSTANT <std::string> STRING_LITERAL SIZEOF
%token PTR_OP <std::string> INC_OP <std::string> DEC_OP <std::string> LE_OP <std::string> GE_OP <std::string> EQ_OP <std::string> NE_OP
%token <std::string> AND_OP <std::string> OR_OP <std::string> XOR_OP
%token TYPE_NAME

%token INT FLOAT BOOL VOID CONST

%token IF ELSE WHILE DO CONTINUE BREAK RETURN

%token SEMICOLON
%token <std::string> COMMA
%token <std::string> EQ
%token CURLY_OPEN
%token CURLY_CLOSE
%token ROUND_OPEN
%token ROUND_CLOSE
%token BRACKET_OPEN
%token BRACKET_CLOSE
%token <std::string> STOP
%token <std::string> AND
%token <std::string> EXCLAMATION
%token <std::string> TILDE
%token <std::string> DEC
%token <std::string> INC
%token <std::string> MULTIPLY
%token <std::string> DIVIDE
%token <std::string> MODULO
%token <std::string> LESS
%token <std::string> GREATER
%token <std::string> CARET
%token <std::string> OR

%token  PRINT

/* definition return types for grammar rules */
%type <CTM::EType> type_specifier
%type <CTM::EType> type_qualifier
%type <std::vector<CTM::EType>> specifier_qualifier_list
%type <CTM::VariableIdentifier> parameter_declaration
%type <std::vector<VariableIdentifier>> parameter_list
%type <std::vector<VariableIdentifier>> parameter_type_list

%type <std::pair<std::string, bool>> init_declarator			//
%type <std::vector<std::pair<std::string, bool>>> init_declarator_list	//	?!?!?!?!?!
%type <std::pair<std::string, CTM::EExp>> constant			//

%type <std::pair<std::string, CTM::EExp>> additive_expression
%type <std::pair<std::string, CTM::EExp>> multiplicative_expression
%type <std::pair<std::string, CTM::EExp>> primary_expression
%type <std::pair<std::string, CTM::EExp>> postfix_expression
%type <std::vector<std::pair<std::string, CTM::EExp>>> argument_expression_list
%type <std::pair<std::string, CTM::EExp>> unary_expression
%type <std::pair<std::string, CTM::EExp>> cast_expression
%type <std::pair<std::string, CTM::EExp>> expression
%type <std::pair<std::string, CTM::EExp>> shift_expression
%type <std::pair<std::string, CTM::EExp>> relational_expression
%type <std::pair<std::string, CTM::EExp>> equality_expression
%type <std::pair<std::string, CTM::EExp>> and_expression
%type <std::pair<std::string, CTM::EExp>> exclusive_or_expression
%type <std::pair<std::string, CTM::EExp>> inclusive_or_expression
%type <std::pair<std::string, CTM::EExp>> logical_and_expression
%type <std::pair<std::string, CTM::EExp>> logical_or_expression
%type <std::pair<std::string, CTM::EExp>> conditional_expression
%type <std::pair<std::string, CTM::EExp>> assignment_expression

%locations

/* start of grammar for our language */
%start translation_unit

%%
/* first called rule */
translation_unit
	: external_declaration 
	| translation_unit external_declaration
	;

external_declaration
	: function_definition 
	     	{
		    driver.setIsFunctionDeclaration(false); //TODO kdyztak vratit delalo my to problem pri deklaraci promenych volalo se to dvakrat:: MARIL
         	}
	| declaration
	;

/* definition of function */
function_definition
	: specifier_qualifier_list IDENTIFIER ROUND_OPEN parameter_type_list ROUND_CLOSE 
			{
				driver.setIsFunctionDeclaration(true);
				
				if($1.size() != 1) {
					//TODO end - error
					std::cerr << "moc typovych specifieru" << std::endl;
  					YYERROR;
				}
				CTM::FunctionIdentifier fi($1[0], $2, $4, true);
				std::string fiOrdinaryName = fi.ordinaryName();
				driver.setVarSpace(fiOrdinaryName);
				if(driver.haveDefinedFunctionIdentifier(fi)) {
					std::cerr << "function definition already exists" << std::endl;
  					YYERROR;
				}
				driver.pushFunctionIdentifier(fi);
				for(std::vector<VariableIdentifier>::reverse_iterator it = $4.rbegin(); it != $4.rend(); it++) {
					std::vector<EType> tmp;
					if(it->isConstant()) {
						tmp.push_back(EType::CONST);
					}
					tmp.push_back(it->type());
					std::string tmpStr(it->identifier());				
					driver.pushVariableIdentifier(tmpStr, tmp, true, true);
				}
			}
		compound_statement 
			{
				driver.jumpOutOfFunction();
				driver.clearVarSpace();
				driver.setIsFunctionDeclaration(false);
				
			} 
	| specifier_qualifier_list IDENTIFIER ROUND_OPEN ROUND_CLOSE 
			{
				driver.setIsFunctionDeclaration(true);
				
				if($1.size() != 1) {
				//TODO end - error
					std::cerr << "moc typovych specifieru" << std::endl;
  					YYERROR;
				}
				std::vector<CTM::VariableIdentifier> tmp;
				CTM::FunctionIdentifier fi($1[0], $2, tmp, true);
				std::string fiOrdinaryName = fi.ordinaryName();
				driver.setVarSpace(fiOrdinaryName);
				if(driver.haveDefinedFunctionIdentifier(fi)) {
					std::cerr << "function definition already exists" << std::endl;
					YYERROR;
				}
				driver.pushFunctionIdentifier(fi);
			} 
		compound_statement 
			{
				driver.jumpOutOfFunction();
				driver.clearVarSpace();
				driver.setIsFunctionDeclaration(false);
				
			}
	;

/* declaration of function */
function_declaration
	: specifier_qualifier_list IDENTIFIER ROUND_OPEN parameter_type_list ROUND_CLOSE SEMICOLON
		{			
			if($1.size() != 1) {
				//TODO end - error
				std::cerr << "moc typovych specifieru" << std::endl;
  				YYERROR;
			}
			
			driver.pushFunctionIdentifier(CTM::FunctionIdentifier($1[0], $2, $4, false));
		} 
	| specifier_qualifier_list IDENTIFIER ROUND_OPEN ROUND_CLOSE SEMICOLON
		{
			if($1.size() != 1) {
				//TODO end - error
				std::cerr << "moc typovych specifieru" << std::endl;
  				YYERROR;
			}
			std::vector<CTM::VariableIdentifier> tmp;
			driver.pushFunctionIdentifier(CTM::FunctionIdentifier($1[0], $2, tmp, false));
		} 
	;

primary_expression
	: IDENTIFIER { $$ = std::make_pair($1, CTM::EExp::VAR); driver.setConstant(std::make_pair($1, CTM::EExp::VAR)); }
	| constant { driver.setConstant($1); $$ = $1; } 
	| ROUND_OPEN  expression ROUND_CLOSE {$$ = $2; }
	;

/* values of constants */
constant
	: I_CONSTANT { $$ = std::make_pair($1, CTM::EExp::INT); }	
	| F_CONSTANT { $$ = std::make_pair($1, CTM::EExp::FLOAT); }
	| B_CONSTANT { $$ = std::make_pair($1, CTM::EExp::BOOL); }
	;

string
	: STRING_LITERAL
	;

postfix_expression
	: primary_expression {$$ = $1;}
	| postfix_expression BRACKET_OPEN I_CONSTANT BRACKET_CLOSE{$$ = $1; driver.setLastIndex($3);}
	| IDENTIFIER ROUND_OPEN ROUND_CLOSE 
		{
			std::stringstream ss;
			ss << "_" << $1;
			$$ = std::make_pair(ss.str(), CTM::EExp::FUNC);
			driver.setConstant($$);
			driver.callFunction($1);
			
		}
	| IDENTIFIER ROUND_OPEN argument_expression_list ROUND_CLOSE
		{
			std::stringstream ss;
			ss << "_" << $1;
			for(std::pair<std::string, CTM::EExp> el : $3) {
				if(el.second == EExp::VAR) {
					VariableIdentifier vi = driver.findVariable(el.first);
					if(vi.positionStack() == -1) {
						error(@3, "Variable not declared");
						YYERROR;
					}
					EType type = vi.type();
					ss << "_" << toString(type);
				}
				else if(el.second == EExp::FUNC) {
					FunctionIdentifier fi = driver.findFunctionByOrdinary(el.first);
					EType type = fi.returnType();
					ss << "_" << toString(type);
				}
				else {
					ss << "_" << toString(el.second);
				}	
			}
			$$ = std::make_pair(ss.str(), CTM::EExp::FUNC);
			driver.setConstant($$);
			driver.callFunction($1, $3);
		}
	| postfix_expression INC_OP 
	| postfix_expression DEC_OP 
	| ROUND_OPEN type_name ROUND_CLOSE CURLY_OPEN initializer_list CURLY_CLOSE 
	| ROUND_OPEN type_name ROUND_CLOSE CURLY_OPEN initializer_list COMMA CURLY_CLOSE  
	;

argument_expression_list
	: assignment_expression {$$.push_back($1);}
	| argument_expression_list COMMA assignment_expression 
		{
			$$ = $1;
			$$.push_back($3);
		}
	;

unary_expression
	: postfix_expression {$$ = $1;}
	| INC_OP unary_expression
	| DEC_OP unary_expression
	| unary_operator cast_expression
			{
				$$ = driver.exclamationOP($2);
				driver.setConstant($$);

			}
	| SIZEOF unary_expression
	| SIZEOF ROUND_OPEN type_name ROUND_CLOSE 
	;

unary_operator
	: AND 
	| MULTIPLY 
	| INC 
	| DEC 
	| TILDE 
	| EXCLAMATION 
	;

/* expression cast */
cast_expression
	: unary_expression {$$ = $1;}
	| ROUND_OPEN type_specifier ROUND_CLOSE cast_expression 
		{

			$$ = driver.castOp($2, $4);
			driver.setConstant($$);
		}
	;

multiplicative_expression
	: cast_expression {$$ = $1;}
	| multiplicative_expression MULTIPLY cast_expression
				{
					std::cout << "Nasobeni "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.mulOp($1, $3);
					driver.setConstant($$);
				}
	| multiplicative_expression DIVIDE cast_expression
				{
					std::cout << "Deleni "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.divOp($1, $3);
					driver.setConstant($$);
				}
	| multiplicative_expression MODULO cast_expression
	;

additive_expression
	: multiplicative_expression{$$ = $1;}
	| additive_expression INC multiplicative_expression
				{
					std::cout << "Scitani "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.addOp($1, $3);
					driver.setConstant($$);
				}
	| additive_expression DEC multiplicative_expression
				{
					std::cout << "Odcitani "<< $1.first << " " << $3.first << std::endl;
					$$ =driver.subOp($1, $3);
					driver.setConstant($$);
				}
	;

shift_expression
	: additive_expression { $$ = $1;}
	;

relational_expression 
	: shift_expression { $$ = $1;} 
	| relational_expression LESS shift_expression
	| relational_expression GREATER shift_expression
	| relational_expression LE_OP shift_expression
	| relational_expression GE_OP shift_expression
	;

equality_expression
	: relational_expression { $$ = $1;}
	| equality_expression EQ_OP relational_expression
				{
					std::cout << "Equality "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.eqOp($1, $3);
					driver.setConstant($$);
				}
	| equality_expression NE_OP relational_expression
				{
					std::cout << "Nonequality "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.nonEqOp($1, $3);
					driver.setConstant($$);
				}
	;

and_expression
	: equality_expression { $$ = $1;}
	| and_expression AND equality_expression				
	;

exclusive_or_expression
	: and_expression { $$ = $1;}
	| exclusive_or_expression CARET and_expression
	;

inclusive_or_expression
	: exclusive_or_expression { $$ = $1;}
	| inclusive_or_expression OR exclusive_or_expression		
	;

logical_and_expression
	: inclusive_or_expression { $$ = $1;}
	| logical_and_expression AND_OP inclusive_or_expression
				{
					std::cout << "And "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.andOp($1, $3);
					driver.setConstant($$);
				}
	;

logical_or_expression
	: logical_and_expression { $$ = $1;}
	| logical_or_expression OR_OP logical_and_expression
				{
					std::cout << "Or "<< $1.first << " " << $3.first << std::endl;
					$$ = driver.orOp($1, $3);
					driver.setConstant($$);
				}
	;

conditional_expression
	: logical_or_expression { $$ = $1;}
	;

assignment_expression
	: conditional_expression { $$ = $1;}
	| unary_expression EQ assignment_expression 
				{
					driver.setLastIdent($1.first);		
				}
	;

/* Processing of expressions, checks for operation's order */
expression
	: assignment_expression { $$ = $1;}
	| expression COMMA assignment_expression
	;

/* declaration */
declaration
	: specifier_qualifier_list init_declarator_list SEMICOLON 
		{ 
		    for (std::pair<std::string, bool> dec : $2) {
		         driver.pushVariableIdentifier(dec.first,$1,dec.second);
		    }
		}
	| function_declaration
	;

init_declarator_list
	: init_declarator { $$.push_back($1);}
	| init_declarator_list COMMA init_declarator { $$ = $1; $$.push_back($3);}
	;

init_declarator
	: IDENTIFIER BRACKET_OPEN constant BRACKET_CLOSE EQ initializer
		{
			driver.setLastArraySize($3.first);
			$$ = std::make_pair($1, true);
		}
	| IDENTIFIER BRACKET_OPEN constant BRACKET_CLOSE
		{  
			driver.setLastArraySize($3.first);
			$$ = std::make_pair($1, false);
		}
	| IDENTIFIER EQ initializer
		{
			$$ = std::make_pair($1,true);
		}
	| IDENTIFIER
		{
			$$ = std::make_pair($1, false);
		}
	;

/* declaration of type and if it is const or not */
type_specifier
	: VOID { $$ = EType::VOID; driver.setLastType(EType::VOID);}
	| INT	{ $$ = CTM::EType::INT; driver.setLastType(EType::INT);}
	| FLOAT	{ $$ = CTM::EType::FLOAT; driver.setLastType(EType::FLOAT);}
	| BOOL	{ $$ = CTM::EType::BOOL; driver.setLastType(EType::BOOL);}
	;

specifier_qualifier_list
	: type_specifier specifier_qualifier_list { $$ = $2; $$.push_back($1);}
	| type_specifier { $$.push_back($1);}
	| type_qualifier specifier_qualifier_list { $$ = $2; $$.push_back($1);}
	| type_qualifier { $$.push_back($1);}
	;

type_qualifier
	: CONST { $$ = CTM::EType::CONST; }
	;

type_qualifier_list
	: type_qualifier
	| type_qualifier_list type_qualifier
	;

parameter_type_list
	: parameter_list { $$ = $1;}
	;

parameter_list
	: parameter_declaration  { $$.push_back($1); }
	| parameter_list COMMA parameter_declaration { $$ = $1; $$.push_back($3); }
	;

/* declaration of type and name */
parameter_declaration
	: specifier_qualifier_list IDENTIFIER { $$ = VariableIdentifier::fromVectorFactory($2, $1, -1, false); }
	| specifier_qualifier_list { }
	;

type_name
	: specifier_qualifier_list abstract_declarator
	| specifier_qualifier_list
	;

abstract_declarator
	: direct_abstract_declarator
	;

direct_abstract_declarator
	: ROUND_OPEN abstract_declarator ROUND_CLOSE
	| BRACKET_OPEN BRACKET_CLOSE
	| BRACKET_OPEN MULTIPLY BRACKET_CLOSE
	| BRACKET_OPEN type_qualifier_list assignment_expression BRACKET_CLOSE
	| BRACKET_OPEN type_qualifier_list BRACKET_CLOSE
	| BRACKET_OPEN assignment_expression BRACKET_CLOSE
	| direct_abstract_declarator BRACKET_OPEN BRACKET_CLOSE
	| direct_abstract_declarator BRACKET_OPEN MULTIPLY BRACKET_CLOSE
	| direct_abstract_declarator BRACKET_OPEN type_qualifier_list assignment_expression BRACKET_CLOSE
	| direct_abstract_declarator BRACKET_OPEN type_qualifier_list BRACKET_CLOSE
	| direct_abstract_declarator BRACKET_OPEN assignment_expression BRACKET_CLOSE
	| ROUND_OPEN ROUND_CLOSE
	| ROUND_OPEN parameter_type_list ROUND_CLOSE
	| direct_abstract_declarator ROUND_OPEN ROUND_CLOSE
	| direct_abstract_declarator ROUND_OPEN parameter_type_list ROUND_CLOSE
	;

/* inicialization of variables and arrays */
initializer
	: CURLY_OPEN initializer_list CURLY_CLOSE
	| assignment_expression
	;

initializer_list
	: initializer 
	| initializer_list COMMA initializer
	;

/* block of code */
statement
	: print_statement  
	| compound_statement
	| expression_statement {driver.assigmentExppresion();}
	| selection_statement 
	| iteration_statement 
	| jump_statement
	;

compound_statement
	: CURLY_OPEN CURLY_CLOSE
	| CURLY_OPEN block_item_list CURLY_CLOSE
	;

block_item_list
	: block_item
	| block_item_list block_item
	;

/* block of code inside { } */
block_item
	: declaration
	| statement
	;

expression_statement
	: SEMICOLON
	| expression SEMICOLON
	;

print_statement
	:PRINT STRING_LITERAL SEMICOLON { driver.printString($2); } 
	|PRINT expression SEMICOLON { driver.printValue($2); } 
	;

/* condition if - else if - else */
selection_statement
	: if if_selection_statement compound_statement else_selection_statement
	| if if_selection_statement compound_statement { driver.startLabelNext(); }
	;

if_selection_statement
	: ROUND_OPEN expression ROUND_CLOSE { driver.labelNext(); }
	;

else_selection_statement
	: else compound_statement { driver.startLabelEnd(); }
	| elseif if_selection_statement compound_statement else_selection_statement
	;

if
	: IF
	;

elseif
	: ELSE IF { driver.labelEnd(true); driver.startLabelNext(); }
	;

else
	: ELSE { driver.labelEnd(false); }
	;

/* while loop */
iteration_statement
  : while_statement statement { driver.labelWhile(); driver.startWhileEnd(); }
	| DO statement WHILE ROUND_OPEN expression ROUND_CLOSE SEMICOLON
	;

while_statement
	: while ROUND_OPEN expression ROUND_CLOSE { driver.whileExpression(); }
  ;

while
	: WHILE { driver.startLabelWhile(); }
  ;

/* continue and break jump for while and return jump for functions */
jump_statement
	: CONTINUE SEMICOLON { driver.checkIfWhile(); driver.labelWhile(); }
	| BREAK SEMICOLON { driver.checkIfWhile(); driver.whileBreak(); }
	| RETURN SEMICOLON
		{
			driver.jumpOutOfFunction();
		}
	| RETURN expression SEMICOLON
		{
			driver.jumpOutOfFunction($2);
		}
 

%%
/* end of grammmar rules */

/* error message with code line */
void 
CTM::Parser::error( const location_type &l, const std::string &err_message )
{
   std::cerr << "\033[1;31m" /*RED_COLOR*/ << "Error: " << "\033[0m" /*DEFAULT_COLOR*/ << err_message << " at " << l << "\n";
}
