#include "Driver.h"
 
CTM::Driver::Driver() :
	ADriver(),
	actualFunction("")
{	
	floatIsContained = false;
	setIsFunctionDeclaration(false);
	std::vector<VariableIdentifier> empty;
	std::string name("main");
	FunctionIdentifier mainFunction(EType::INT, name, empty, false);
	pushFunctionIdentifier(mainFunction);

	lastArraySize = -1;
	lastLocalSP = 0;
	stackPointer = 0;
	globalPointer = 0;
	lastFindIndexStack = 0;
	cntPrintString = 0;
	nextCounter = 0;
	endCounter = 0;
	whileCounter = 0;
	expressionJumpCounter = 0;
	std::vector<int> lNextCounter;
	std::vector<int> lEndCounter;
  std::vector<int> lWhileCounter;
	
	std::string defaultVarSpace = "_main";	
	setVarSpace(defaultVarSpace);
}

CTM::Driver::~Driver() {
}

void CTM::Driver::parse( const char * const filename ) {
   if(!filename) {
	   exit( EXIT_FAILURE);
   }
   std::ifstream inFile( filename );
   if( ! inFile.good() ) {
       exit( EXIT_FAILURE );
   }
   parseStream( inFile );
   
   for(std::map<std::string, std::vector<FunctionIdentifier>>::iterator it = declaredFunctions.begin(); it != declaredFunctions.end(); ++it) {
  	for(std::vector<FunctionIdentifier>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
  		if(!it2->defined()) {
        std::string err = "Missing definition of function ";
        err += it2->ordinaryName(); //TODO lepsi vypis (jmeno funkce)
        printError(err);
  		}
  	}
   }

   while(!identifierStack.empty()) {
  	std::cout << identifierStack.top() << std::endl;
  	identifierStack.pop();
   }

   /*for(std::map<std::string, std::unique_ptr<VariableIdentifier>>::iterator it = declaredGlobalVariables.begin(); it != declaredGlobalVariables.end(); ++it) {
	std::cout << it->first << " " << it->second->isConstant << " " << ((it->second->type == EType::INT) ? "INT" : "NECO INE") << std::endl; 
   }*/
}


void CTM::Driver::printFileHeader() {
  /* outputFileData << ".data" << std::endl;
   outputFile << ".text" << std::endl;
   outputFile << ".globl main" << std::endl;
   outputFile << "main:" << std::endl;
   outputFile << "li $v0,10" << std::endl; // systemove volani - konec programu
   outputFile << "syscall" << std::endl;*/

   //outPutDataStack.push_back(".data");
}

void CTM::Driver::saveOutPutFile(){
	outputFile << ".globl main" << std::endl;
	outputFile << ".data" << std::endl;
	std::stringstream ss;
	std::rewind(outDataStringSegmentFile);
	std::rewind(outDataSegmentFile);
	std::rewind(outCodeSegmentFile);
  char buffer [256];

	do {
    	if (!std::fgets(buffer,256, outDataSegmentFile)) break;
		  ss << buffer;
  } while (strlen(buffer)>1);

   outputFile  << ss.str();
	 ss.str("");
	 ss.clear();
 	 outputFile << "es: .space 100"<< std::endl;
	 do {
   		if (!std::fgets(buffer,256,outDataStringSegmentFile)) break;
		  ss << buffer;
  } while (strlen(buffer)>1);

  outputFile  << ss.str();
	ss.str("");
	ss.clear();
       
	outputFile << ".text" << std::endl;
	outputFile << "main:" << std::endl;
	outputFile << " \tlui $s6, 0x1001" << std::endl;
	outputFile << " \tla $s4, es" << std::endl;
	outputFile << "\tmove $s7, $sp" << std::endl;
	outputFile << "\tjal _main" << std::endl;
	outputFile << "\tli $v0, 10" << std::endl;
	outputFile << "\tsyscall" << std::endl;
	do {
    	if (!std::fgets(buffer,256,outCodeSegmentFile)) break;
		  ss << buffer;
  } while (strlen(buffer)>1);

  outputFile  << ss.str();
	ss.str("");
	ss.clear();	
}

void CTM::Driver::pushIdentifier(std::string identifier) {
	identifierStack.push(identifier);
}

/* print error and exit translation */
void CTM::Driver::printError(std::string msg) {
  std::cerr << "\033[1;31m" /*RED_COLOR*/ << "Error: " << "\033[0m" /*DEFAULT_COLOR*/ << msg << "\n";
	exit(1);
}

/*  */
std::string CTM::Driver::createErrMsg(std::string msg1, std::string msg2, std::string msg3, std::string msg4) {
	std::ostringstream oss;
  oss << msg1 << msg2 << msg3 << msg4;
	return oss.str();
}

void CTM::Driver::pushType(EType type) {
	typeStack.push(type);
}

 void CTM::Driver::printString(std::string value){
	std::string name;
	std::stringstream ss;
	ss  << "_f_"<< cntPrintString++;
	name = ss.str();
	ss << ": .asciiz " << value; 
	 ss << std::endl;  std::fputs(ss.str().data(),outDataStringSegmentFile);
	
      
	sysPrintString(name);	
	
}

void CTM::Driver::printValue(std::pair<std::string, CTM::EExp> value){
	if(value.second == EExp::VAR){
		VariableIdentifier varIdent = findVariable(value.first);
		EType t = varIdent.type();

		if(varIdent.positionStack() == -1) {
      			printError(createErrMsg("Variable ", lastIdent, " is not declared", ""));
		}

		
			
		if(varIdent.sizeArray() == -1) {
			//normal 
			
			if(varIdent.isGlobal())	{
			    	int index =varIdent.positionStack();	

			    	if(varIdent.type() == EType::INT){
			    		sysPrintVariableInt("$s6",index);
			    	}else if(varIdent.type() == EType::BOOL){
			       		sysPrintVariableBool("$s6",index);
  			    	}else if(varIdent.type() == EType::FLOAT){
			       		sysPrintVariableFloat("$s6",index);
  			    	}
			}
			else {
			    	addiInt("$s5", "$s7",  -varIdent.positionStack());
			    	//int index = stackPointer - varIdent.positionStack() ;	
				//std::cout << " POLE LOCAL :  " <<stackPointer<< " -- "<< varIdent.positionStack()<< " -- "<< index << std::endl;
			    	if(varIdent.type() == EType::INT){
			    		sysPrintVariableInt("$s5",-varIdent.getSize());
				}else if(varIdent.type() == EType::BOOL){
				       sysPrintVariableBool("$s5",-varIdent.getSize());
  			    	}
				else if(varIdent.type() == EType::FLOAT){
			       		sysPrintVariableFloat("$s5",-varIdent.getSize());
  				}
			}	
		}
		else {
			if(lastIndex.empty()){
        printError(createErrMsg("Bad ", value.first, " array declared", ""));
			}			

			int idx = lastIndex.top();
			lastIndex.pop();

			if((idx > varIdent.sizeArray() - 1) || (idx < 0)){
        printError("Index to array has bad value");
			}
			
			//pole
			
			if(varIdent.isGlobal())	{
				 int index = varIdent.positionStack() + ( (idx ) * varIdent.getSize());
				 if(varIdent.type() == EType::INT){
					
			    		sysPrintVariableInt("$s6",index);
			    	}else if(varIdent.type() == EType::BOOL){
			       		sysPrintVariableBool("$s6",index);
  			    	}else if(varIdent.type() == EType::FLOAT){
			       		sysPrintVariableFloat("$s6",index);
  			    	}
			}
			else {
				 
				 addiInt("$s5", "$s7",  -varIdent.positionStack());
				  // idx = (varIdent.sizeArray()) - idx;
				if(varIdent.type() == EType::INT){
			    		sysPrintVariableInt("$s5",(-(idx+1)*varIdent.getSize()));
			    	}else if(varIdent.type() == EType::BOOL){
			       		sysPrintVariableBool("$s5",(-(idx+1)*varIdent.getSize()));
  			    	}else if(varIdent.type() == EType::FLOAT){
			       		sysPrintVariableFloat("$s5",(-(idx+1)*varIdent.getSize()));
  			   	}
			}
		}
	} 
	else if(value.second == EExp::FUNC){
		std::cout << "TODO Driver::printValue -> EExp::FUNC" << std::endl;
	}
	else if(value.second == EExp::INT ){
	     	sysPrintInt(stoi(value.first));
	} else if(value.second == EExp::BOOL){
		if(value.first.compare("true") == 0 || value.first.compare("1") == 0){
	
       		 	sysPrintInt(1);
    	        }else if(value.first.compare("false") == 0 || value.first.compare("0") == 0){

 			 sysPrintInt(0);
     		}else{
			     printError("Bad bool value");
     		}
	}else if(value.second == EExp::FLOAT){
		sysPrintFloat(value.first);
	}
}

bool CTM::Driver::haveDefinedFunctionIdentifier(const FunctionIdentifier &identifier) {
	std::map<std::string, std::vector<FunctionIdentifier>>::iterator it = declaredFunctions.find(identifier.name());	
	if(it == declaredFunctions.end()) {
		return false;
	}
	for(CTM::FunctionIdentifier found : it->second) {
		if(!(identifier.ordinaryName().compare(found.ordinaryName()))) {
			if(found.defined()) {
				return true;
			}
			else {
				return false;
			}
		}
	}
	return false;
}


void CTM::Driver::pushFunctionIdentifier(const CTM::FunctionIdentifier &identifier){
	//declaredFunctions[identifier.name()].push_back(identifier);
	std::map<std::string, std::vector<FunctionIdentifier>>::iterator it = declaredFunctions.find(identifier.name());

	std::stringstream ss;	
	if(it == declaredFunctions.end()) {
		declaredFunctions[identifier.name()].push_back(identifier);
		if(identifier.defined()) {
			ss << identifier.ordinaryName() << ":" << std::endl;
                        std::fputs(ss.str().data(),outCodeSegmentFile);
		}
		return;
	}
	for(std::vector<CTM::FunctionIdentifier>::iterator it2 = it->second.begin(); it2 != it->second.end(); ++it2) {
		if(!(identifier.ordinaryName().compare(it2->ordinaryName()))) {
			if(it2->defined()) {
				return;
			}
			else {
				it->second.erase(it2);
				it->second.push_back(identifier);
				ss << identifier.ordinaryName() << ":" << std::endl;
                                std::fputs(ss.str().data(),outCodeSegmentFile);
				ss.str("");
				ss.clear();
				return;
			}
		}
	}

	
	it->second.push_back(identifier);
}


void CTM::Driver::setLastArraySize(std::string size){
	int value;
  try{
	 value = stoi(size); //TODO mozna bude dobre osetrit zapornou velikost pole
	 lastArraySize = value;
   } catch (...){
	     printError("Bad array declaration");
   }
}


void CTM::Driver::pushVariableIdentifier(std::string &name, std::vector<EType> &types, bool haveValue){
	pushVariableIdentifier(name, types, haveValue, false);

}

void CTM::Driver::pushVariableIdentifier(std::string &name, std::vector<EType> &types, bool haveValue, bool declaredVariable){
			
	//switch
	if(isFunctionDeclaration){
		std::map<std::string, VariableIdentifier> mapVariable = declaredLocalStack[varSpace].back();

		//Prohledani celeho stacku
		/*for(int i = 0; i < declaredLocalStack[varSpace].size(); i++){
			std::map<std::string, VariableIdentifier> temp = declaredLocalStack.at(i);
			if ( temp.find(name) != temp.end() ) {
  				// already exists
          printError(createErrMsg("Variable ", name, " already exists", ""));
			}
		}*/
		//Prohledani vrcholu stacku
		if ( mapVariable.find(name) != mapVariable.end() ) {
  			// already exists
        		printError(createErrMsg("Variable ", name, " already exists", ""));
		}
		 VariableIdentifier ident = VariableIdentifier::fromVectorFactory(name,types, stackPointer ,false);
		 
		 if(lastArraySize < 0){
 			for(int i = 0;i < valueStack.size();i++){
				ident.pushStackValues(valueStack[i]);
			}
			
			ident.setHaveData(haveValue);
			ident.setSizeArray(lastArraySize);

			
			
			//ident.setPositionStack(stackPointer);
			if(declaredVariable) {
				stackPointer+= ident.getSize();
			}
			else {
				
                        	localVariableDeclared(ident,haveValue);	
			}
			
			
			mapVariable[name] = ident; 
			valueStack.clear();
                        declaredLocalStack[varSpace].pop_back();
		        pushLocalStack(mapVariable); // musel jsem jinak my maze mapu nevim dopič proč :: Maril
			
			
			
        	}else{ 
			for(int i = 0;i < valueStack.size();i++){
				ident.pushStackValues(valueStack[i]);
			}
			
			ident.setHaveData(haveValue);
			ident.setSizeArray(lastArraySize);

			localArrayDeclared(ident,haveValue);
			//ident.setPositionStack(stackPointer);	
			mapVariable[name] = ident; 
			valueStack.clear();
			
  			declaredLocalStack[varSpace].pop_back();
			pushLocalStack(mapVariable); // musel jsem jinak my maze mapu nevim dopič proč :: Maril
		} 
 
	}else {
		if ( declaredGlobalVariables.find(name) != declaredGlobalVariables.end() ) {
  		// already exists
      			printError(createErrMsg("Variable ", name, " already exists", ""));
		}

		declaredGlobalVariables[name] = VariableIdentifier::fromVectorFactory(name,types, globalPointer,true); //-99 označuje že je to globální
		declaredGlobalVariables[name].setSizeArray(lastArraySize);
		for(int i = 0;i < valueStack.size();i++){
			declaredGlobalVariables[name].pushStackValues(valueStack[i]);
		}
		declaredGlobalVariables[name].setHaveData(haveValue);
		
		
		if(lastArraySize < 0){
			switch(declaredGlobalVariables[name].type()){
					case EType::INT:
					  printGlobalIntIdentifer(declaredGlobalVariables[name],haveValue); globalPointer += 4;
					break;
				case EType::FLOAT:
					  printGlobalFloatIdentifer(declaredGlobalVariables[name],haveValue); globalPointer += 4;
					break;
				case EType::BOOL:
				  	printGlobalBoolIdentifer(declaredGlobalVariables[name],haveValue); globalPointer += 4;
				break;
		   	}
        	}else{
			 
			switch(declaredGlobalVariables[name].type()){
				case EType::INT:
					  printGlobalIntArrayIdentifer(declaredGlobalVariables[name],haveValue);globalPointer += 4*lastArraySize;
					break;
				case EType::FLOAT:
					  printGlobalFloatArrayIdentifer(declaredGlobalVariables[name],haveValue);globalPointer += 4*lastArraySize;
					break;
				case EType::BOOL:
				  	  printGlobalBoolArrayIdentifer(declaredGlobalVariables[name],haveValue);globalPointer += 4*lastArraySize;
				break;
		   	}
		}valueStack.clear();
	}
	
     
	lastArraySize = -1;

	
}

void CTM::Driver::initBoolToReg(std::string reg,std::string value){

	
 if(value.compare("true") == 0 ){
        li(reg, "1");
     }else if(value.compare("false") == 0 ){
 	    li(reg, "0");
     }else{
	     if(value.empty()){
		move(reg,"$t0");
	     }
	     else{
	     	printError("Bad bool value");
            }
     }
}

void CTM::Driver::localArrayDeclared(VariableIdentifier &variable, bool haveValue){
 	std::stringstream ss; 
	std::string reg = "$t0";

	int sizeArray = variable.sizeArray();
	
	if(haveValue){	
		std::cout << sizeArray <<  " -- "  << variable.getStackSize();
		if(sizeArray!= variable.getStackSize()){
			printError("Bad array declaration");
		}
		for(int i = 0;i <  sizeArray;i++){
		   std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(i);
		   if(lastStackValue.second != EExp::VAR){
		           if(variable.type() == EType::BOOL){
				
				if(lastStackValue.second != EExp::BOOL){
					printError("Bad bool value");
				}
				moveToStack(-4);
				//addiInt("$sp", "$sp", -1);stackPointer+=1;
		     		initBoolToReg(reg,lastStackValue.first);
				sb(reg, "$sp", 0);
			    }else if(variable.type() == EType::INT){
				if(lastStackValue.second != EExp::INT){
					printError("Bad int value");
				}
				moveToStack(-4);
				//addiInt("$sp", "$sp", -4);stackPointer+=4;
				li(reg, lastStackValue.first);
				sw(reg, "$sp", 0);
	   		    }else if(variable.type() == EType::FLOAT){
				if(lastStackValue.second != EExp::FLOAT){
					printError("Bad float value");
				}
				moveToStack(-4);
				//addiInt("$sp", "$sp", -4);stackPointer+=4;
				reg = "$f0";
			    	liFloat(reg, lastStackValue.first);
				saveFloat(reg, "$sp", 0);
			     }
		     }else{
                        saveLocalValue(variable, lastStackValue);
		     }
		  
		}
		
	}else{
		for(int i = 0;i <  sizeArray;i++){
			if(variable.type() == EType::BOOL){
				moveToStack((-4)*sizeArray);
				//addiInt("$sp", "$sp", (-1)*sizeArray);stackPointer+=1;
		     

			}else if(variable.type() == EType::INT){
				moveToStack((-4)*sizeArray);
				//addiInt("$sp", "$sp", (-4)*sizeArray);stackPointer+=4;
	   		}else if(variable.type() == EType::FLOAT){
				moveToStack((-4)*sizeArray);
			    	//addiInt("$sp", "$sp", (-4)*sizeArray);stackPointer+=4;
				reg = "$f0";
			}
		}
	}
	std::fputs(ss.str().data(),outCodeSegmentFile);
	
}

void CTM::Driver::saveLocalValue(VariableIdentifier &variable,std::pair<std::string, CTM::EExp> lastStackValue){
		std::string reg = "$t0";
		if(variable.type() == EType::BOOL){
			//addiInt("$sp", "$sp", -1);stackPointer+=1;
			moveToStack(-4);
		}else if(variable.type() == EType::INT){
			
			moveToStack(-4); 
			//addiInt("$sp", "$sp", -4);stackPointer+=4;
		}else if(variable.type() == EType::FLOAT){
			
		    	moveToStack(-4);
			//addiInt("$sp", "$sp", -4);stackPointer+=4;
			reg = "$f0";
		}
		VariableIdentifier varIdent = findVariable(lastStackValue.first);
		if(varIdent.positionStack() == -1){
      			printError(createErrMsg("Variable ", lastIdent, " is not declared", ""));
		}
		
    			printError(createErrMsg("Variable ", lastIdent, " is not declared", ""));
		//TODO LOAD FROM varIdent
		if(varIdent.sizeArray() == -1){
			if(varIdent.isGlobal()){
				//normal global
				int index =varIdent.positionStack();
				lw(reg,"$s6",index);
				sw(reg, "$sp", 0);			
		
			}
			else{
				//normal local
				 addiInt("$s5", "$s7",  -varIdent.positionStack());
				lw(reg,"$s5",-varIdent.getSize());
				 sw(reg, "$sp", 0);
			}

		}else{
	
			if(lastIndex.empty()){
        			printError("");
			}			

			int idx = lastIndex.top();
			lastIndex.pop();

			if((idx > varIdent.sizeArray() - 1) || (idx < 0)){
				printError("Index to array has bad value");
			}

			if(varIdent.isGlobal()){
				//array global
				 int index = varIdent.positionStack() + ( (idx ) * varIdent.getSize()) ;
				lw(reg,"$s6",index);
				sw(reg, "$sp", 0);	
			}
			else{
				//array local
				 addiInt("$s5", "$s7",  -varIdent.positionStack());			
				 lw(reg,"$s5",(-(idx+1)*varIdent.getSize()));
				 sw(reg, "$sp", 0);
			}
		}
			   
}

void CTM::Driver::localVariableDeclared(VariableIdentifier &variable, bool haveValue){

	std::stringstream ss; 
	std::string reg = "$t0";
  	//variable.popBackStackValues(); // TODO smazat - jen vypis, stara metoda
	if(haveValue) {	
		std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(variable.getStackSize()-1);
		if(!lastStackValue.first.empty()) {	
			if(lastStackValue.second == EExp::BOOL){
				if(variable.type() != EType::BOOL) {
					printError("Bad bool value");

				}
				moveToStack(-4);
				//addiInt("$sp", "$sp", -1);
		     		initBoolToReg(reg,lastStackValue.first);
				//stackPointer+=1;
				sb(reg, "$sp", 0);
			}
			else if(lastStackValue.second == EExp::INT){
				if(variable.type() != EType::INT) {
					printError("Bad int value");
				}
				moveToStack(-4);
				//addiInt("$sp", "$sp", -4);
				//stackPointer+=4;
				li(reg, lastStackValue.first);
				sw(reg, "$sp", 0);
	   		} 
			else if(lastStackValue.second == EExp::FLOAT){
				if(variable.type() != EType::FLOAT) {
					printError("Bad float value");
				}
				moveToStack(-4);
				//addiInt("$sp", "$sp", -4);
				//stackPointer+=4;
				reg = "$f0";
			    	liFloat(reg, lastStackValue.first);

				saveFloat(reg, "$sp", 0);	

			}
			else if(lastStackValue.second == EExp::VAR) {
				saveLocalValue(variable, lastStackValue);
			}
			else if(lastStackValue.second == EExp::FUNC) { // Function
				std::string destReg = "$s3";
				FunctionIdentifier fi = findFunctionByOrdinary(lastStackValue.first);
				if(fi.returnType() == EType::FLOAT) {
					destReg = "$f4";
				}
				moveToStack(-4);
				move(reg, destReg);
				sw(reg, "$sp", 0);
			}	
		  
		}
		else { //expression
			if(lastStackValue.second == EExp::FLOAT) {
				moveToStack(-4);	
		    		swc1("$f0", "$sp", 0);
		 	}
			else {
				moveToStack(-4);	
		    		sw("$t0", "$sp", 0);
			}
		    	
		}
		
	}
	else { // without value
		if(variable.type() == EType::BOOL) {
			//addiInt("$sp", "$sp", -1);stackPointer+=1;
	     		moveToStack(-4);

		}else if(variable.type() == EType::INT) {
			moveToStack(-4);
			//addiInt("$sp", "$sp", -4);stackPointer+=4;
   		}else if(variable.type() == EType::FLOAT) {
		    	moveToStack(-4);
			//addiInt("$sp", "$sp", -4);stackPointer+=4;
			//reg = "$f0";
		}
	}
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::moveToStack(int index){

 	
        addiInt("$sp", "$sp", index);
	
	stackPointer-= index;
	
}

void CTM::Driver::setConstant(std::pair<std::string, CTM::EExp> value){
        valueStack.push_back(value);
}

void CTM::Driver::printGlobalIntArrayIdentifer(VariableIdentifier &variable, bool haveValue){
   std::stringstream ss; 
    if(!haveValue){
	     ss  << " .space " << (4*(lastArraySize));
       ss << std::endl;  std::fputs(ss.str().data(),outDataSegmentFile);
	     return;
    }
    ss  << " .word ";
    
     if(variable.getStackSize()!= variable.sizeArray()){
    	printError("Bad int array declaration");
    	return;
     }

     for(int i = 0;i < variable.sizeArray();i++){
     	std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(i);
     	if(lastStackValue.second == CTM::EExp::INT){
    		ss << lastStackValue.first;
    		if(i!= variable.sizeArray()-1){
    			ss << ", ";
    		}
      } else{
  		  printError("Bad int value");
      }
    }
    
    ss << std::endl;  std::fputs(ss.str().data(),outDataSegmentFile);
}

void CTM::Driver::printGlobalFloatArrayIdentifer(VariableIdentifier &variable, bool haveValue){
     std::stringstream ss;

     if(!haveValue){
	     ss  << " .space " << (4*(lastArraySize));
     	 ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
	     return;
     }
	
     ss  << " .float ";
    
     if(variable.getStackSize()!= variable.sizeArray()){
	     printError("Bad float array declaration");
	     return;
     }

     for(int i = 0;i < variable.sizeArray();i++){
     	std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(i);
     	if(lastStackValue.second == CTM::EExp::FLOAT ||lastStackValue.second == CTM::EExp::INT){
		
  		ss << lastStackValue.first;
  		if(i!= variable.sizeArray()-1){
  			ss << ", ";
  		}
      } else{
  		   printError("Bad float value");
      }
     }
     ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
}

void CTM::Driver::printGlobalBoolArrayIdentifer(VariableIdentifier &variable, bool haveValue){
     std::stringstream ss;
     if(!haveValue){
    	ss  << " .space " << lastArraySize;
    	ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
    	return;
     }

     ss  << " .byte ";
    
     if(variable.getStackSize()!= variable.sizeArray()){
	     printError("Bad float array declaration");
	     return;
     }

     for(int i = 0;i < variable.sizeArray();i++){
       std::pair<std::string, CTM::EExp>  lastStackValue = variable.popStackValues(i);
    	 if(lastStackValue.first.compare("true") == 0 || lastStackValue.first.compare("1") == 0){
    		ss << 1;
    	 }else if(lastStackValue.first.compare("false") == 0 || lastStackValue.first.compare("0") == 0){
    		ss << 0;
    	 }else{
    		printError("Bad bool value");
    	 }

  	  if(i!= variable.sizeArray()-1){
  		  ss << ", ";
  	  }
    }

    ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
}

void CTM::Driver::printGlobalIntIdentifer(VariableIdentifier &variable, bool haveValue){
      std::stringstream ss;    
      if(!haveValue){
     
        ss    << " .word 0";
         ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
	return;
     }
	 ss   << " .word "; 
    std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(0);
     	if(lastStackValue.second == CTM::EExp::INT){
		
		ss << lastStackValue.first;
		
     	} else{
		  printError("Bad int value");
     	}	 
  ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
}
void CTM::Driver::printGlobalFloatIdentifer(VariableIdentifier &variable, bool haveValue){
     std::stringstream ss; 
     if(!haveValue){
       ss    << " .float 0.";
        ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
	return;
     }
      ss    << " .float ";
     std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(0);
     	if(lastStackValue.second == CTM::EExp::FLOAT ||lastStackValue.second == CTM::EExp::INT){
		
		ss << lastStackValue.first;
		
     	} else{
		    printError("Bad float value");
     	} 
  ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
}
void CTM::Driver::printGlobalBoolIdentifer(VariableIdentifier &variable, bool haveValue){
     
	std::stringstream ss; 
     if(!haveValue){
        ss     << " .byte 0";
        ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
	return;
     }

     std::pair<std::string, CTM::EExp> lastStackValue = variable.popStackValues(0);

     if(lastStackValue.first.compare("true") == 0 || lastStackValue.first.compare("1") == 0){
	
        ss       << " .byte 1";
          ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
     }else if(lastStackValue.first.compare("false") == 0 || lastStackValue.first.compare("0") == 0){

 	 ss    << " .byte 0";
  	 ss << std::endl; std::fputs(ss.str().data(),outDataSegmentFile);
     }else{
	     printError("Bad bool value");
     }
}

void CTM::Driver::jumpOutOfFunction() {
	valueStack.clear();
	jr("$ra");
}

void CTM::Driver::jumpOutOfFunction(std::pair<std::string, EExp> &returnStatement) {
	if(returnStatement.second == EExp::VAR) {
		VariableIdentifier found = findVariable(returnStatement.first);
		if(found.positionStack() == -1) {
			std::cerr << "TODO Error - var not found" << std::endl;
		}
		EType foundType = found.type();
		std::string sp = (found.isGlobal()) ? "$s6" : "$s7";
		int sign = (found.isGlobal()) ? 1 : -1;
		int index = (found.sizeArray() == -1) ? 1 : (lastIndex.top()+1);
		if(found.isGlobal()) {
			index--;
		}
		lastIndex.pop();
		addiInt("$s5", sp, sign * found.positionStack());
		if(foundType == EType::INT) {
			moveToStack(-4);
			lw("$s0", "$s5", index * found.getSize()*sign);					
			sw("$s0", "$sp", 0);
		}
		else if(foundType == EType::FLOAT) {
			lw("$f0", "$s5", index * found.getSize()*sign);
			swc1("$f0", "$sp", 0);
		}
		else if(foundType == EType::BOOL) {
			li("$s0", returnStatement.first);	// TODO lb, sb
			sb("$s0", "$sp", 0);			//
		}

		/*VariableIdentifier found = findVariable(it->first);
			if(found.positionStack() == -1){
				std::cout << "TODO err -> callFunction()" << std::endl;
			}
			EType foundType = found.type();
			std::string sp = (found.isGlobal()) ? "$s6" : "$s7";
			int sign = (found.isGlobal()) ? 1 : -1;
			int index = (found.sizeArray() == -1) ? 1 : (lastIndex.top()+1);
			std::cout << "index\t" << index << std::endl;
			if(found.isGlobal()) {
				index--;
			}
			lastIndex.pop();
			addiInt("$s5", sp, sign * found.positionStack());
			if(foundType == EType::INT) {
				moveToStack(-4);
				lw("$s0", "$s5", index * found.getSize()*sign);					
				sw("$s0", "$sp", 0);

			}
			else if(foundType == EType::FLOAT) {
				moveToStack(-4);
				lwc1("$f0", "$s5", index * found.getSize()*sign);					
				saveFloat("$f0", "$sp", 0);
			}
			else if(foundType == EType::BOOL) {
				moveToStack(-4);
				lw("$s0", "$s5", index * found.getSize()*sign);					
				sb("$s0", "$sp", 0);
			}*/
	}
	else if(returnStatement.second == EExp::FUNC){
		std::cerr << "TODO Error - zakazat funce" << std::endl;
		
	}
	else if(returnStatement.first.empty()) {
		if(returnStatement.second == EExp::INT) {
			sw("$t0", "$s7", -4);
		}
		else if(returnStatement.second == EExp::FLOAT) {
			swc1("$f0", "$s7", -4);
		}
		else if(returnStatement.second == EExp::BOOL) {
			sw("$t0", "$s7", -4);			//
		}
	}
	else {
		if(returnStatement.second == EExp::INT) {
			li("$s0", returnStatement.first);
			sw("$s0", "$s7", -4);
		}
		else if(returnStatement.second == EExp::FLOAT) {
			liFloat("$f0", returnStatement.first);
			swc1("$f0", "$s7", -4);
		}
		else if(returnStatement.second == EExp::BOOL) {
			li("$s0", returnStatement.first);	// TODO lb, sb
			sw("$s0", "$s7", -4);			//
		}
	}
	
	jumpOutOfFunction();
}

void CTM::Driver::callFunction(std::string &functionName) {
	std::stringstream ss;
	ss << "_" << functionName;
	std::string ordinaryName = ss.str();
	FunctionIdentifier fi = findFunctionByOrdinary(ordinaryName);
	//std::cout << "TODO Driver::callFunction() -> Not found" << std::endl;
	addiInt("$sp", "$sp", -8);
	sw("$ra", "$sp", 4);
	sw("$s7", "$sp", 0);
	move("$s7", "$sp");
	jal(ordinaryName);
	move("$sp", "$s7");
	if(fi.returnType() == EType::INT) {
		lw("$s3", "$sp", -4);
	}
	else if(fi.returnType() == EType::FLOAT) {
		lwc1("$f4", "$sp", -4);
	}
	else if(fi.returnType() == EType::BOOL) {
		lw("$s3", "$sp", -4);
	}
	lw("$s7", "$sp", 0);
	lw("$ra", "$sp", 4);
	addiInt("$sp", "$sp", 8);
	
}

void CTM::Driver::callFunction(std::string &functionName, std::vector<std::pair<std::string, CTM::EExp>> &parameters) {
	std::vector<EType> parameterTypes;
	for(std::pair<std::string, CTM::EExp> el : parameters) {
		
		if(el.second == EExp::BOOL) {
			parameterTypes.push_back(EType::BOOL);
		}
		else if(el.second == EExp::INT) {
			parameterTypes.push_back(EType::INT);
		}
		else if(el.second == EExp::FLOAT) {
			parameterTypes.push_back(EType::FLOAT);
		}
		else if(el.second == EExp::VAR) {
			VariableIdentifier found = findVariable(el.first);
			if(found.positionStack() == -1){
				printError("Variable not exist");
			}
			
			EType foundType = found.type(); // Zjistim typ
			parameterTypes.push_back(foundType);
			
			
			
		}
		else if(el.second == EExp::FUNC) {
			FunctionIdentifier fi = findFunctionByOrdinary(el.first);
			parameterTypes.push_back(fi.returnType());
		}else{
		}
	}
	std::string finding = FunctionIdentifier::getOrdinaryName(functionName, parameterTypes);
	std::cout << finding << std::endl;
	FunctionIdentifier fi = findFunctionByOrdinary(finding);
	
	addiInt("$sp", "$sp", -8);
	sw("$ra", "$sp", 4);
	sw("$s7", "$sp", 0);
	for(std::vector<std::pair<std::string, CTM::EExp>>::reverse_iterator it = parameters.rbegin(); it != parameters.rend(); ++it) {
		
		if(it->second == EExp::INT) {
			addiInt("$sp", "$sp", -4);
			if(it->first.empty()){
				getFromStack("$s0", CTM::EType::INT);
			}
			else{
				li("$s0", it->first);					
			}
			sw("$s0", "$sp", 0);
		}
		else if(it->second == EExp::FLOAT) {
			addiInt("$sp", "$sp", -4);
			if(it->first.empty()){
				getFromStack("$f0", CTM::EType::FLOAT);
			}
			else{
				liFloat("$f0", it->first);	
			}				
			saveFloat("$f0", "$sp", 0);
		}
		else if(it->second == EExp::BOOL) {
			addiInt("$sp", "$sp", -4);
			if(it->first.empty()){
				getFromStack("$s0", CTM::EType::BOOL);
			}
			else{
				li("$s0", it->first);					
			}				
			sb("$s0", "$sp", 0);
		}
		else if(it->second == EExp::VAR) {
			VariableIdentifier found = findVariable(it->first);
			if(found.positionStack() == -1){
				std::cout << "TODO err -> callFunction()" << std::endl;
			}
			EType foundType = found.type();
			std::string sp = (found.isGlobal()) ? "$s6" : "$s7";
			int sign = (found.isGlobal()) ? 1 : -1;
			int index = (found.sizeArray() == -1) ? 1 : (lastIndex.top()+1);
			if(found.isGlobal()) {
				index--;
			}
			lastIndex.pop();
			addiInt("$s5", sp, sign * found.positionStack());
			if(foundType == EType::INT) {
				addiInt("$sp", "$sp", -4);
				lw("$s0", "$s5", index * found.getSize()*sign);					
				sw("$s0", "$sp", 0);

			}
			else if(foundType == EType::FLOAT) {
				addiInt("$sp", "$sp", -4);
				lwc1("$f0", "$s5", index * found.getSize()*sign);					
				saveFloat("$f0", "$sp", 0);
			}
			else if(foundType == EType::BOOL) {
				addiInt("$sp", "$sp", -4);
				lw("$s0", "$s5", index * found.getSize()*sign);					
				sb("$s0", "$sp", 0);
			}
			
		}
		else if(it->second == EExp::FUNC){
			std::cerr << "TODO - Driver::callFunction(string &, vector<pair<string, EExp>> &) Zakazat funkce" << std::endl;
		}
		else {
			std::cerr << "TODO - Driver::callFunction(string &, vector<pair<string, EExp>> &)" << std::endl
				<< "\tfor(FunctionIdentifier el : map<FunctionIdentifier>)" << std::endl;
		}
	}
	addiInt("$s7", "$sp", 4*parameters.size());
	jal(finding);
	move("$sp", "$s7");
	if(fi.returnType() == EType::INT) {
		lw("$s3", "$sp", -4);
	}
	else if(fi.returnType() == EType::FLOAT) {
		lwc1("$f4", "$sp", -4);
	}
	else if(fi.returnType() == EType::BOOL) {
		lw("$s3", "$sp", -4);
	}
	lw("$s7", "$sp", 0);
	lw("$ra", "$sp", 4);
	addiInt("$sp", "$sp", 8);
}


/*void CTM::Driver::createVariableIdentifer() {
	bool isConst = false;
	
	EType type;
	if(typeStack.top() == EType::CONST) {
		isConst = true;
		type = typeStack.top();
		typeStack.pop();
	}
	else {
		type = typeStack.top();
		typeStack.pop();
		if(typeStack.top() == EType::CONST) {
			isConst = true;
			typeStack.pop();
		}
	}

	declaredGlobalVariables[identifierStack.top()] = std::unique_ptr<VariableIdentifier>(new VariableIdentifier(identifierStack.top(), type, isConst));
	//declaredGlobalVariables[identifierStack.top()] = *new VariableIdentifier(identifierStack.top(), type, isConst);
	identifierStack.pop();

}*/

void CTM::Driver::pushLocalStack(std::map<std::string, VariableIdentifier>& nova){
	declaredLocalStack[varSpace].push_back(nova);
}
void CTM::Driver::setIsFunctionDeclaration(bool value){
	isFunctionDeclaration = value;
	stackPointer = 0;
	if(value == true){
		std::map<std::string, VariableIdentifier> nova;
		
		pushLocalStack(nova);
	
	}
       
}

void CTM::Driver::setLastIdent(std::string lastIdent){
	this->lastIdent = lastIdent;
}
void CTM::Driver::setLastType(CTM::EType lastType){
	this->lastType = lastType;
}
void CTM::Driver::setLastIndex(std::string index){
	
	lastIndex.push(stoi(index));
}
void CTM::Driver::getFromStack(std::string reg, CTM::EType type){
	/*outputFile << "lw " << reg <<", 0($sp)" << " #Load part result from stack" << std::endl ;
	outputFile << "addi $sp,$sp,4" << std::endl; */

	if(type == CTM::EType::INT){
		lw(reg, "$s4", 0);
		addiInt("$s4", "$s4", -4);		
		
	}
	if(type == CTM::EType::FLOAT){
		lwc1(reg, "$s4", 0);
		addiInt("$s4", "$s4", -4);
		
	}
	if(type == CTM::EType::BOOL){
		lb(reg, "$s4", 0);
		addiInt("$s4", "$s4", -4);		
		
	}
	lastPartResult = CTM::EType::VOID;
	//addiInt("$sp", "$sp", 4);

}
void CTM::Driver::setToStack(std::string reg, CTM::EType type){
	/*outputFile << "addi $sp,$sp,-4" << " #Save part result to stack"<< std::endl;
	outputFile << "sw " << reg <<", 0($sp)" << std::endl;*/

	//addiInt("$sp", "$sp", -4);
	if(type == CTM::EType::INT){
		addiInt("$s4", "$s4", 4);			
		sw(reg, "$s4", 0);
		lastPartResult = CTM::EType::INT;
		
	}
	if(type == CTM::EType::FLOAT){
		addiInt("$s4", "$s4", 4);
		swc1(reg, "$s4", 0);
		lastPartResult = CTM::EType::FLOAT;
		
	}
	if(type == CTM::EType::BOOL){
		addiInt("$s4", "$s4", 4);
		sb(reg, "$s4", 0);
		lastPartResult = CTM::EType::BOOL;

		
	}
}


std::string CTM::Driver::deterPartResult(std::string value, std::string reg, CTM::EExp type , std::string otherValue, CTM::EExp otherType){

	if(value.empty()){ 
		std::cout << "Last part type " << toString(lastPartResult) << " " << toString(type)  << std::endl; 
		if(resolveType(type, value) == CTM::EExp::INT){
			if(resolveType(otherType, otherValue) == CTM::EExp::INT){
				getFromStack(reg, CTM::EType::INT);
			}
			else{
          			printError(createErrMsg("Bad type of operators ", toString(otherType), " and ", toString(type)));
			}
		}
		else if(resolveType(type, value) == CTM::EExp::BOOL){
				if(resolveType(otherType, otherValue) == CTM::EExp::BOOL){
					getFromStack(reg, CTM::EType::BOOL);
				}else{
          				printError(createErrMsg("Bad type of operators ", toString(otherType), " and ", toString(type)));
				}
			}
		else{


		}
		return reg;

	}
	else{

		if(type == EExp::VOID) {
			std::cout << "TODO - cant access return value of VOID" << std::endl;
		}
		else if(type == EExp::VAR){ // FLOAT<???
			VariableIdentifier varIdent = findVariable(value);
		
			if(varIdent.positionStack() == -1){
        			printError(createErrMsg("Variable ", lastIdent, " is not declared", ""));
			}

			VariableIdentifier varIdent2 = findVariable(otherValue);
			if(otherType == CTM::EExp::VAR){			
				if(varIdent2.positionStack() == -1){
          				printError(createErrMsg("Variable ", otherValue, " is not declared", ""));
				}
				if(varIdent.type() != varIdent2.type()){
					printError("Variables has different types");
				}
			}

			if(varIdent.sizeArray() == -1){
			//normal;
				if(varIdent.isGlobal()){
					int index =varIdent.positionStack();
					if(varIdent.type() == EType::INT) {
						lw(reg, "$s6",index);
					}
					else if(varIdent.type() == EType::FLOAT) {
						lwc1(reg, "$s6",index);
					}
					else if(varIdent.type() == EType::BOOL) {
						lw(reg, "$s6",index);
					}
				}
				else{
					addiInt("$s5", "$s7", -varIdent.positionStack());
					if(varIdent.type() == EType::INT) {
						lw(reg, "$s5", -varIdent.getSize());
					}
					else if(varIdent.type() == EType::FLOAT) {
						lwc1(reg, "$s5", -varIdent.getSize());
					}
					else if(varIdent.type() == EType::BOOL) {
						lw(reg, "$s5", -varIdent.getSize());
					}
				}
			}else{
			//pole
				int idx = lastIndex.top();
				lastIndex.pop();
				if((idx > varIdent.sizeArray() - 1) || (idx < 0)){
					printError("Index to array has bad value");
				}
				if(varIdent.isGlobal()){
					int index = varIdent.positionStack() + ( (idx ) * varIdent.getSize());
					if(varIdent.type() == EType::INT) {
						lw(reg, "$s6",index);
					}
					else if(varIdent.type() == EType::FLOAT) {
						lwc1(reg, "$s6",index);
					}
					else if(varIdent.type() == EType::BOOL) {
						lw(reg, "$s6",index);
					}
				}else{
				 	
					addiInt("$s5", "$s7", -varIdent.positionStack());
					if(varIdent.type() == EType::INT) {
						lw(reg, "$s5", (-(idx+1)*varIdent.getSize()));
					}
					else if(varIdent.type() == EType::FLOAT) {
						lwc1(reg, "$s5", (-(idx+1)*varIdent.getSize()));
					}
					else if(varIdent.type() == EType::BOOL) {
						lw(reg, "$s5", (-(idx+1)*varIdent.getSize()));
					}
				}
			}
		}
		else if(type == EExp::FUNC) {
			FunctionIdentifier fi = findFunctionByOrdinary(value);
			if(fi.returnType() == EType::INT) {
				move(reg, "$s3");
			}
			if(fi.returnType() == EType::FLOAT) {
				move(reg, "$f4");
			}
			if(fi.returnType() == EType::BOOL) {
				move(reg, "$s3");
			}
		}
		else {
			if(type == CTM::EExp::INT){
				li(reg, value);
			}else if(type == CTM::EExp::FLOAT){
				liFloat(reg, value);
			}else if(type == CTM::EExp::BOOL){
				li(reg, value);
			}else{

			}
			return reg;
		}
	}	

	return reg;	
}

std::string prepareValue(std::pair<std::string, CTM::EExp> value){
	if(value.second == CTM::EExp::INT){
		
	}else if(value.second == CTM::EExp::FLOAT){
		//floatIsContained = true;

	}else if(value.second == CTM::EExp::VAR){

	}
}

CTM::VariableIdentifier CTM::Driver::findVariable(std::string var){
	
	VariableIdentifier ident;
	//Local
	for(int i = declaredLocalStack[varSpace].size() - 1; i >= 0; i--){
		std::map<std::string, VariableIdentifier> current = declaredLocalStack[varSpace].at(i);
		
		if ( current.find(var) != current.end() ) {
  			// found!!
		        lastFindIndexStack = i;
			return current[var];		
		}
	
	}
	//Global
	if ( declaredGlobalVariables.find(var) != declaredGlobalVariables.end() ) {
  		// found!!		
		return declaredGlobalVariables[var];		
	}

	//not found
	return ident;
	
}

CTM::FunctionIdentifier CTM::Driver::findFunctionByOrdinary(std::string &func){
	for(std::pair<std::string, std::vector<FunctionIdentifier>> it : declaredFunctions) {
		if(func.find(it.first) != std::string::npos) {
			for(FunctionIdentifier it2 : it.second) {
				if(!it2.ordinaryName().compare(func)) {
					return it2;
				}
			}
		}
	}
	printError("Unable to find function");
}

std::pair<std::string, CTM::EExp> CTM::Driver::castOp(CTM::EType type, std::pair<std::string, CTM::EExp> operator2){
	if(operator2.first.empty())printError("Cast operation is not possible");		
	//NUMBERS	
	//VAR
	if(operator2.second == CTM::EExp::VAR){
		VariableIdentifier varIdent = findVariable(operator2.first);
		if(varIdent.positionStack() == -1){
      			printError(createErrMsg("Variable ", operator2.first, " is not declared", ""));
		}
		//std::cerr << "Cast var " <<  operator2.first<< "  " << varIndent.posiion<< std::endl; 
		if(varIdent.sizeArray() == -1){
			//normal;
			if(varIdent.type() == CTM::EType::INT){
				
				operator2.second = CTM::EExp::INT;
				
				if(varIdent.isGlobal()){
					int index =varIdent.positionStack();	
					lw("$t0", "$s6", index);
				}
				else{
					addiInt("$s5", "$s7",  -varIdent.positionStack());
					lw("$t0", "$s5", -varIdent.getSize());
				}
			}
			if(varIdent.type() == CTM::EType::FLOAT){
				operator2.second = CTM::EExp::FLOAT;
				if(varIdent.isGlobal()){
					int index =varIdent.positionStack();	
					lwc1("$f0", "$s6", index);
				}
				else{
					addiInt("$s5", "$s7",  -varIdent.positionStack());
					lwc1("$f0", "$s5", -varIdent.getSize());
				}
			}
		}else{
			//pole
			int idx = lastIndex.top();
			lastIndex.pop();
			if((idx > varIdent.sizeArray() - 1) || (idx < 0)){
				printError("Index to array has bad value");
			}
			if(varIdent.type() == CTM::EType::INT){
				
				operator2.second = CTM::EExp::INT;
				if(varIdent.isGlobal()){
					int index = varIdent.positionStack() + ( (idx + 1) * varIdent.getSize()) -1;
					lw("$t0", "$s6", index);
				}
				else{
					addiInt("$s5", "$s7",  -varIdent.positionStack());
					lw("$t0", "$s5", (-(idx+1)*varIdent.getSize()));
				}
			}
			if(varIdent.type() == CTM::EType::FLOAT){
				operator2.second = CTM::EExp::FLOAT;
				if(varIdent.isGlobal()){
					int index = varIdent.positionStack() + ( (idx + 1) * varIdent.getSize()) -1;
					lwc1("$f0", "$s6", index);
				}
				else{
					addiInt("$s5", "$s7",  -varIdent.positionStack());
					lwc1("$f0", "$s5", (-(idx+1)*varIdent.getSize()));
				}
			}
		}
		
	}else if(operator2.second == CTM::EExp::INT){
		li("$t0", operator2.first);
	}else if(operator2.second == CTM::EExp::FLOAT){
		liFloat("$f0", operator2.first);

	}else if(operator2.second == CTM::EExp::BOOL){
		li("$t0", operator2.first);

	}else if(operator2.second == CTM::EExp::FUNC){
		FunctionIdentifier fi = findFunctionByOrdinary(operator2.first);
		EType t = fi.returnType();
		operator2.second = convert(t);
		if(t == EType::INT) {
				move("$t0", "$s3");
		}
		if(t == EType::FLOAT) {
				move("$f0", "$f4");
		}
		if(t == EType::BOOL) {
				move("$t0", "$s3");
		}
	}

	if((type == CTM::EType::INT) && (operator2.second == CTM::EExp::FLOAT)){
		cvtFloatToInt("$f0", "$f0");
		mfc1("$t0", "$f0");
		setToStack("$t0", CTM::EType::INT);
	}
	if((type == CTM::EType::FLOAT) && (operator2.second == CTM::EExp::INT)){
		mtc1("$t0", "$f0");
		cvtIntToFloat("$f0", "$f0");
		setToStack("$f0", CTM::EType::FLOAT);
	}
	if((type == CTM::EType::INT) && (operator2.second == CTM::EExp::INT)){
		setToStack("$t0", CTM::EType::INT);
	}
	if((type == CTM::EType::FLOAT) && (operator2.second == CTM::EExp::FLOAT)){
		setToStack("$f0", CTM::EType::FLOAT);
	}
	if((type == CTM::EType::BOOL) && (operator2.second == CTM::EExp::INT)){		
		move("$t1","$t0");
		li("$t0", "1");
		
		std::stringstream ss;
		ss << "bool_cast_" << expressionJumpCounter++;
		bne("$zero", "$t0", ss.str());
		li("$t0", "0");
		ss << ":" << std::endl;
		std::fputs(ss.str().data(),outCodeSegmentFile);
		setToStack("$t0", CTM::EType::BOOL);
		

	}
	if((type == CTM::EType::INT) && (operator2.second == CTM::EExp::BOOL)){
		setToStack("$t0", CTM::EType::INT);		
	}
	if((type == CTM::EType::BOOL) && (operator2.second == CTM::EExp::FLOAT)){
		printError(createErrMsg("Cant cast: ", toString(type), " from " , toString(operator2.second)));
	}
	if((type == CTM::EType::FLOAT) && (operator2.second == CTM::EExp::BOOL)){
		printError(createErrMsg("Cant cast: ", toString(type), " from " , toString(operator2.second)));
		
	}

	
	EExp t = convert(type);
	return std::make_pair("", t);
}
void CTM::Driver::assigmentExppresion(){
	if(!lastIdent.empty()) {
		VariableIdentifier varIdent = findVariable(lastIdent);

		if(varIdent.positionStack() == -1) {
     			printError(createErrMsg("Variable ", lastIdent, " is not declared", ""));
		}
		else if(varIdent.isConstant()) {
			printError(createErrMsg("Variable ", lastIdent, " is constant", ""));
		}
		if(valueStack.size() >= 1) { 
			std::pair<std::string, EExp> input = valueStack[valueStack.size()-1];
			if(!input.first.empty()) { //expression
				EExp tmp = input.second;
				std::cout << "TODO - line 1520: ['" << input.first << "', '" << toString(tmp) << "']" << std::endl;
				if(input.second == EExp::INT) {
					li("$t0", valueStack[valueStack.size()-1].first);
				}
				else if(input.second == EExp::FLOAT) {
					liFloat("$f0", valueStack[valueStack.size()-1].first);
				}
				else if (input.second == EExp::BOOL) {
					li("$t0", valueStack[valueStack.size()-1].first);
				}
				else if(input.second == EExp::VAR) {
					VariableIdentifier vi = findVariable(input.first);
					if(vi.type() != varIdent.type()) {
						printError(createErrMsg("Variable ", varIdent.identifier(), " has different type than input", "")); 
					}
					if(vi.isGlobal()) {
						int index = vi.positionStack();
						if(vi.type() == EType::INT) {
							lw("$t0", "$s6", index);
						}
						else if(vi.type() == EType::FLOAT) {
							lwc1("$f0", "$s6", index);
						}
						else if (vi.type() == EType::BOOL) {
							lw("$t0", "$s6", index);
						}
					} else {
						addiInt("$s5", "$s7",  -vi.positionStack());
						if(vi.type() == EType::INT){	
							lw("$t0", "$s5", -vi.getSize());
						}
						else if(vi.type() == EType::FLOAT){
							lwc1("$f0", "$s5", -vi.getSize());
						}
						else if(vi.type() == EType::BOOL){
							lw("$t0", "$s5", -vi.getSize());
						}
					}
					
				}
				else if(input.second == EExp::FUNC) {
					FunctionIdentifier fi = findFunctionByOrdinary(input.first);
					if(fi.returnType() != varIdent.type()) {
						printError(createErrMsg("Variable ", varIdent.identifier(), " has different type than input", "")); 
					}					
					if(fi.returnType() == EType::INT) {
						lw("$s0", "$s7", -4);
					}
					else if(fi.returnType() == EType::FLOAT) {
						lwc1("$f4", "$s7", -4);
					}
					else if (fi.returnType() == EType::BOOL) {
						lw("$s0", "$s7", -4);
					}
				}
				
			}

			varIdent.setHaveData(true);
			if(varIdent.isGlobal()){
				int index = varIdent.positionStack();
				if(input.second == EExp::INT) {
					sw("$t0", "$s6",index);
				}
				else if(input.second == EExp::FLOAT) {
					swc1("$f0", "$s6",index);
				}
				else if (input.second == EExp::BOOL) {
					sb("$t0", "$s6",index);
				}
			
			}
			else {
				addiInt("$s5", "$s7",  -varIdent.positionStack());
				int index = -varIdent.positionStack();
				if(varIdent.type() == CTM::EType::INT){	
					sw("$t0", "$s5", -varIdent.getSize());
				}
				else if(varIdent.type() == CTM::EType::FLOAT){
					swc1("$f0", "$s5", -varIdent.getSize());
				}
				else if(varIdent.type() == CTM::EType::BOOL){
					sb("$f0", "$s5", -varIdent.getSize());
				}
			}
			
		}		
		else {
			
			//EExp tmp = valueStack[valueStack.size()-1].second;
			std::cout << "TODO - Neznamy stav" << std::endl;
			//if(valueStack[valueStack.size()-1].second == EExp::FLOAT) {
			//	liFloat("$f0", valueStack[0].first);
			//}
			//else {
			//	li("$t0", valueStack[valueStack.size()-1].first);
			//}
		}
		
		/*if(varIdent.sizeArray() == -1) { //variable
			varIdent.setHaveData(true);
			int index = stackPointer - varIdent.positionStack() - varIdent.getSize();
			//PRO LOAD BOOL sb
			if(varIdent.isGlobal()){
				int index =varIdent.positionStack();
				sw("$t0", "$s6",index);
			}
			else{
				addiInt("$s5", "$s7",  -varIdent.positionStack());
				if(varIdent.type() == CTM::EType::INT){	
					sw("$t0", "$s5", -varIdent.getSize());
				}
				else if(varIdent.type() == CTM::EType::FLOAT){
					swc1("$f0", "$s5", -varIdent.getSize());
				}
			}
			
		}
		else {

			varIdent.setHaveData(true);
			//pole
			int idx = lastIndex.top();
			lastIndex.pop();
			if((idx > varIdent.sizeArray() - 1) || (idx < 0)){
				printError("Index to array has bad value");
			}
			if(varIdent.isGlobal()){
				int index = varIdent.positionStack() + ( (idx ) * varIdent.getSize());	
				sw("$t0", "$s6",index);		
			}
			else {
				addiInt("$s5", "$s7",  -varIdent.positionStack());
				if(varIdent.type() == CTM::EType::INT){			
					sw("$t0", "$s5", (-(idx+1)*varIdent.getSize()));
				}
				else if(varIdent.type() == CTM::EType::FLOAT){
					swc1("$f0", "$s5", (-(idx+1)*varIdent.getSize()));
				}			
			}

		}*/
		valueStack.clear();
		lastIdent.clear();

	}
}

void CTM::Driver::setVarSpace(std::string & varSpace) {
	this->varSpace = varSpace;
	if(declaredLocalStack.find(varSpace) == declaredLocalStack.end()) {
		std::vector<std::map<std::string, VariableIdentifier>> clean;
		std::map<std::string, VariableIdentifier> cleanMap;	
		clean.push_back(cleanMap);
		declaredLocalStack[varSpace] = clean;
	}
}

void CTM::Driver::clearVarSpace() {
	varSpace.clear();
}


std::pair<std::string, CTM::EExp> CTM::Driver::addOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){

	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	

	if(a == b) {
		if(a == EExp::INT) {
			
			addInt("$t0", deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second));	
			setToStack("$t0", CTM::EType::INT);
		}

		if(a == EExp::FLOAT) {
			addFloat("$f0", deterPartResult(value1.first, "$f1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$f2", value2.second, value1.first, value1.second));
		
			setToStack("$f0", CTM::EType::FLOAT);
		}
		if(a == EExp::BOOL) {
			printError("Cant add bool");
		}
		else {
			std::cout << "TODO - Driver::add()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}
std::pair<std::string, CTM::EExp> CTM::Driver::subOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){	
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	if(a == b) {
		if(a == EExp::INT) {
			
			subInt("$t0", deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second));
					
			setToStack("$t0", CTM::EType::INT);
		}

		if(a == EExp::FLOAT) {
			subFloat("$f0", deterPartResult(value1.first, "$f1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$f2", value2.second, value1.first, value1.second));			
			setToStack("$f0", CTM::EType::FLOAT);
		}
		if(a == EExp::BOOL) {
			printError("Cant sub bool");
		}
		else {
			std::cout << "TODO - Driver::subOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}
std::pair<std::string, CTM::EExp> CTM::Driver::mulOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	if(a == b) {
		if(a == EExp::INT) {
			
			multInt("$t0", deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second));

			setToStack("$t0", CTM::EType::INT);
		}

		if(a == EExp::FLOAT) {
			mulFloat("$f0", deterPartResult(value1.first, "$f1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$f2", value2.second, value1.first, value1.second));
			setToStack("$f0", CTM::EType::FLOAT);
		}
		if(a == EExp::BOOL) {
			printError("Cant mull bool");
		}
		else {
			std::cout << "TODO - Driver::mulOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}
std::pair<std::string, CTM::EExp> CTM::Driver::divOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){	
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	if(a == b) {
		if(a == EExp::INT) {
			
			divInt( deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second));		
			moveLo("$t0");
			setToStack("$t0", CTM::EType::INT);
		}
		if(a == EExp::FLOAT) {
			divFloat("$f0", deterPartResult(value1.first, "$f1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$f2", value2.second, value1.first, value1.second));
			
			//moveLo("$f0");
			setToStack("$f0", CTM::EType::FLOAT);
		}
		if(a == EExp::BOOL) {
			printError("Cant mull bool");
		}
		else {
			std::cout << "TODO - Driver::divOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}
std::pair<std::string, CTM::EExp> CTM::Driver::andOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	std::cout << "AND**** "<< value1.first << " , " << toString(value1.second) << " | " << value2.first << " , "  << toString(value2.second) << std::endl;
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	if(a == b) {

		if(a == EExp::INT) {
			printError(createErrMsg("Cant use logical and with: ", toString(a), ", " , toString(b)));
		}
		if(a == EExp::FLOAT) {
			printError(createErrMsg("Cant use logical and with: ", toString(a), ", " , toString(b)));
		}
		if(a == EExp::BOOL) {
			value1.first = convertBoolValue(value1.first);
			value2.first = convertBoolValue(value2.first);
			andL("$t0", deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second));
			setToStack("$t0", CTM::EType::BOOL);	
		}
		else {
			std::cout << "TODO - Driver::andOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}
std::pair<std::string, CTM::EExp> CTM::Driver::orOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	if(a == b) {
		if(a == EExp::INT) {
			printError(createErrMsg("Cant use logical or with: ", toString(a), ", " , toString(b)));
		}
		if(a == EExp::FLOAT) {
			printError(createErrMsg("Cant use logical or with: ", toString(a), ", " , toString(b)));
		}
		if(a == EExp::BOOL) {
			value1.first = convertBoolValue(value1.first);
			value2.first = convertBoolValue(value2.first);
			orL("$t0", deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second), deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second));
			setToStack("$t0", CTM::EType::BOOL);	
		}
		else {
			std::cout << "TODO - Driver::orOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);

}
std::pair<std::string, CTM::EExp> CTM::Driver::eqOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	std::stringstream ss;
	ss << "equal_op_" << expressionJumpCounter++;
	if(a == b) {
		if(a == EExp::INT) {
			
			std::string firstOperator = deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second);
			std::string secondOperator = deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second);
			li("$t0", "0");
			
			bne(firstOperator, secondOperator, ss.str());
			li("$t0", "1");
			ss << ":" << std::endl;
			std::fputs(ss.str().data(),outCodeSegmentFile);
			setToStack("$t0", CTM::EType::INT);	
		}
		if(a == EExp::FLOAT) {
			std::string firstOperator = deterPartResult(value1.first, "$f1", value1.second, value2.first, value2.second);
			std::string secondOperator = deterPartResult(value2.first, "$f2", value2.second, value1.first, value1.second);
			li("$t0", "0");
			
			c_eq_s(firstOperator, secondOperator);
			bc1f(ss.str());
			li("$t0", "1");
			ss << ":" << std::endl;
			std::fputs(ss.str().data(),outCodeSegmentFile);
			setToStack("$t0", CTM::EType::BOOL);
			
		}
		if(a == EExp::BOOL) {
			value1.first = convertBoolValue(value1.first);
			value2.first = convertBoolValue(value2.first);
			std::string firstOperator = deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second);
			std::string secondOperator = deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second);
			li("$t0", "0");
			
			bne(firstOperator, secondOperator, ss.str());
			li("$t0", "1");
			ss << ":" << std::endl;
			std::fputs(ss.str().data(),outCodeSegmentFile);
			setToStack("$t0", CTM::EType::BOOL);	
		}
		else {
			std::cout << "TODO - Driver::eqOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}
std::pair<std::string, CTM::EExp> CTM::Driver::nonEqOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2){
	EExp a = value1.second, b = value2.second;
	a = resolveType(a, value1.first);
	b = resolveType(b, value2.first);
	
	if((!value1.first.empty() && !value2.first.empty() && (value1.second != CTM::EExp::VAR) && (value2.second != CTM::EExp::VAR) ) && (value1.second != value2.second) && value1.second == EExp::VOID){
		printError("Bad type of constants");
	}
	
	std::stringstream ss;
	ss << "non_equal_op_" << expressionJumpCounter++;
	if(a == b) {
		if(a == EExp::INT) {
			
			std::string firstOperator = deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second);
			std::string secondOperator = deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second);
			li("$t0", "1");
			
			bne(firstOperator, secondOperator, ss.str());
			li("$t0", "0");
			ss << ":" << std::endl;
			std::fputs(ss.str().data(),outCodeSegmentFile);
			setToStack("$t0", CTM::EType::BOOL);	
		}
		if(a == EExp::FLOAT) {
			std::string firstOperator = deterPartResult(value1.first, "$f1", value1.second, value2.first, value2.second);
			std::string secondOperator = deterPartResult(value2.first, "$f2", value2.second, value1.first, value1.second);
			li("$t0", "1");
			
			c_eq_s(firstOperator, secondOperator);
			bc1f(ss.str());
			li("$t0", "0");
			ss << ":" << std::endl;
			std::fputs(ss.str().data(),outCodeSegmentFile);
			setToStack("$t0", CTM::EType::BOOL);
			
		}
		if(a == EExp::BOOL) {
			value1.first = convertBoolValue(value1.first);
			value2.first = convertBoolValue(value2.first);
			std::string firstOperator = deterPartResult(value1.first, "$t1", value1.second, value2.first, value2.second);
			std::string secondOperator = deterPartResult(value2.first, "$t2", value2.second, value1.first, value1.second);
			li("$t0", "1");
			
			bne(firstOperator, secondOperator, ss.str());
			li("$t0", "0");
			ss << ":" << std::endl;
			std::fputs(ss.str().data(),outCodeSegmentFile);
			setToStack("$t0", CTM::EType::BOOL);	
		}
		else {
			std::cout << "TODO - Driver::nonEqOp()" << std::endl;
		}
	}
	else {
		printError(createErrMsg("Bad type of operators: ", toString(a), ", " , toString(b)));
	}
	
	valueStack.clear();
	return std::make_pair("", a);
}

std::pair<std::string, CTM::EExp> CTM::Driver::exclamationOP(std::pair<std::string, CTM::EExp> value){
	if(value.first.empty())printError("Exclamation operation is not possible");
	//NUMBERS	
	std::cout << "Exclamation " << toString(value.second) << " " << value.first << std::endl; 
	//VAR
	if(value.second == CTM::EExp::VAR){
		
		VariableIdentifier varIdent = findVariable(value.first);
		CTM::EType varT = varIdent.type();
		if(varIdent.positionStack() == -1){
      			printError(createErrMsg("Variable ", value.first, " is not declared", ""));
		}
		//std::cerr << "Cast var " <<  value.first<< "  " << varIndent.posiion<< std::endl; 
		if(varIdent.sizeArray() == -1){
			//normal;
			if(varIdent.type() == CTM::EType::BOOL){
				
				value.second = CTM::EExp::BOOL;
				
				if(varIdent.isGlobal()){
					int index =varIdent.positionStack();	
					lw("$t1", "$s6", index);
				}
				else{
					addiInt("$s5", "$s7",  -varIdent.positionStack());
					lw("$t1", "$s5", -varIdent.getSize());
				}
			}
			else{
				value.second = convert(varT);
				std::string type = toString(varT);
				printError(createErrMsg("Exclamation of ", type, " is not possible", ""));
			}
		}else{
			//pole
			int idx = lastIndex.top();
			lastIndex.pop();
			if((idx > varIdent.sizeArray() - 1) || (idx < 0)){
				printError("Index to array has bad value");
			}
			if(varIdent.type() == CTM::EType::BOOL){
				
				value.second = CTM::EExp::BOOL;
				if(varIdent.isGlobal()){
					int index = varIdent.positionStack() + ( (idx + 1) * varIdent.getSize()) -1;
					lw("$t1", "$s6", index);
				}
				else{
					addiInt("$s5", "$s7",  -varIdent.positionStack());
					lw("$t1", "$s5", (-(idx+1)*varIdent.getSize()));
				}
			}
			else{
				value.second = convert(varT);
				std::string type = toString(varT);
				printError(createErrMsg("Exclamation of ", type, " is not possible", ""));
			}
			
		}
		
	}else if(value.second == CTM::EExp::INT){
		std::string type = toString(value.second);
		printError(createErrMsg("Exclamation of ",type, " is not possible", ""));
	}else if(value.second == CTM::EExp::FLOAT){
		std::string type = toString(value.second);
		printError(createErrMsg("Exclamation of ", type, " is not possible", ""));

	}else if(value.second == CTM::EExp::BOOL){
		li("$t1", convertBoolValue(value.first));

	}else if(value.second == CTM::EExp::FUNC){
		
		FunctionIdentifier fi = findFunctionByOrdinary(value.first);
		EType t = fi.returnType();
		value.second = convert(t);
		if(t == EType::INT) {
				
				printError(createErrMsg("Exclamation of ", "INT", " is not possible", ""));
		}
		if(t == EType::FLOAT) {
				
				printError(createErrMsg("Exclamation of ", "FLOAT", " is not possible", ""));
		}
		if(t == EType::BOOL) {
				move("$t1", "$s3");
		}
	}
	std::stringstream ss;
	ss << "exclamation_op_" << expressionJumpCounter++;
	li("$t0", "0");
	li("$t2", "1");		
	bne("$t1", "$t2", ss.str());
	li("$t0", "1");
	ss << ":" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
	setToStack("$t0", CTM::EType::BOOL);		
	
	return std::make_pair("", EExp::BOOL);
}

CTM::EExp CTM::Driver::resolveType(CTM::EExp &type, std::string &name) {
	
	if(type == EExp::VAR) {
		VariableIdentifier id = findVariable(name);
		if(id.positionStack() == -1) {
			std::cerr << "TODO - Unknown Variable " << name << std::endl;
		}
		EType t = id.type();		
		return convert(t);
	}
	else if(type == EExp::FUNC) {
		FunctionIdentifier fi = findFunctionByOrdinary(name);
		EType t = fi.returnType();
		return convert(t);
	}
	return type;
}

std::string CTM::Driver::convertBoolValue(std::string & value){
	if(value.compare("true") == 0){
		return "1";
	}
	else if(value.compare("false") == 0) {
		return "0";
	}
	else {
		//printError(createErrMsg("Bad bool value ", value));
		return value;
	}
}

void CTM::Driver::addInt(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss << "\tadd "<< d << "," << s << "," <<t << " #ADD"<< std::endl;
        std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::addiInt(std::string t,std::string s,int C){
	std::stringstream ss;
	ss << "\taddi " << t <<","<<s<<","<<std::to_string(C)  << std::endl;
        std::fputs(ss.str().data(),outCodeSegmentFile);
}


void CTM::Driver::subInt(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss << "sub " << d<<","<<s<<","<<t << " #SUB"<< std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::multInt(std::string d, std::string s,std::string t){
	std::stringstream ss;
	ss << "mul " << d << ", " << s <<","<<t << " #MUL"<< std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::divInt(std::string s,std::string t){
	std::stringstream ss;
	ss << "div "<<s<<","<<t << " #DIV"<< std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}



void CTM::Driver::addFloat(std::string x,std::string y,std::string z){
	std::stringstream ss;
	ss << "\tadd.s " << x << "," << y << "," << z << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::subFloat(std::string x,std::string y,std::string z){
	std::stringstream ss;
	ss << "sub.s " << x << "," << y << "," << z << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::mulFloat(std::string x,std::string y,std::string z){
	std::stringstream ss;
	ss << "mul.s " << x << "," << y << "," << z << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::divFloat(std::string x,std::string y,std::string z){
	std::stringstream ss;
	ss << "div.s " << x << "," << y << "," << z << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::andL(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss << "and " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::andiL(std::string t,std::string s,int C){
	std::stringstream ss;
	ss << "andi " << t <<","<<s<<","<<std::to_string(C) << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::orL(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss << "or " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::oriL(std::string t,std::string s,int C){
	std::stringstream ss;
	ss << "ori " << t <<","<<s<<","<<std::to_string(C) << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::xorL(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss <<"xor " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::xoriL(std::string t,std::string s,int C){
	std::stringstream ss;
	ss << "xori " << t <<","<<s<<","<<std::to_string(C) << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::nor(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss << "nor " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::slt(std::string d,std::string s,std::string t){
	std::stringstream ss;
	ss << "slt " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::slti(std::string t,std::string s,int C){
	std::stringstream ss;
	ss << "slti " << t <<","<<s<<","<<std::to_string(C)  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}
   
void CTM::Driver::sll(std::string d,std::string t,int shamt){
	std::stringstream ss;
	ss << "sll " << d <<","<<t<<","<<std::to_string(shamt) << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::srl(std::string d,std::string t,int shamt){
	std::stringstream ss;
	ss << "srl " << d <<","<<t<<","<<std::to_string(shamt)  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::sllv(std::string d,std::string t,std::string s){
	std::stringstream ss;
	ss << "sllv " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::srlv(std::string d,std::string t,std::string s){
	std::stringstream ss;
	ss << "srlv " << d<<","<<s<<","<<t << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}  
void CTM::Driver::moveHi(std::string d){
	std::stringstream ss;
	ss << "mfhi " << d << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::moveLo(std::string d){
	std::stringstream ss;
	ss << "\tmflo " << d << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::li(std::string r,std::string s){
	std::stringstream ss;
	ss << "\tli " << r<<","<<s << " #Load immediate"<< std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::liFloat(std::string r,std::string s){
	std::stringstream ss;
	ss << "\tli.s " << r<<","<<s << " #Load immediate"<< std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}


void CTM::Driver::move(std::string r,std::string s){
	std::stringstream ss;
	ss << "\tmove " << r<<","<<s << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::clear(std::string rt){
	std::stringstream ss;
	ss << "move " << rt << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::notP(std::string rt,std::string rs){
	std::stringstream ss;
	ss << "\tnot " << rt<<","<<rs << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}
  
void CTM::Driver::beq(std::string s,std::string t,std::string loop){
	std::stringstream ss;
	ss << "\tbeq " << s <<","<<t<<","<<loop  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::bne(std::string s,std::string t,std::string loop){
	std::stringstream ss;
	ss << "\tbne " << s <<","<<t<<","<<loop  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::bc1t(std::string loop){
	std::stringstream ss;
	ss << "\tbc1t " <<loop  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::bc1f(std::string loop){
	std::stringstream ss;
	ss << "\tbc1f " <<loop  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::c_eq_s(std::string s, std::string d){
	std::stringstream ss;
	ss << "\tc.eq.s " << s <<","<< d  << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::jr(std::string s){
	std::stringstream ss;
	ss << "\tjr " << s << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::jal(std::string &s){
	std::stringstream ss;
	ss << "\tjal " << s << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}



void CTM::Driver::sysPrintInt(int intValue){
     std::stringstream ss;
	ss << "\tli $v0, 1" << std::endl;
     ss << "\tli $a0, " << intValue << std::endl;
     ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}



void CTM::Driver::sysPrintFloat(std::string &floatValue){
	std::stringstream ss;
	ss << "\tli $v0, 2" << std::endl;
     	ss << "\tli.s $f12, " << floatValue << std::endl;
	ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}



void CTM::Driver::sysPrintVariableInt(std::string s, int bytes){
     std::stringstream ss;
     ss << "\tli $v0, 1" << std::endl; 
     ss << "\tlw $a0, "  << bytes << "(" << s << ")" << " #PRINT" << std::endl;
     ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::sysPrintVariableFloat(std::string s, int bytes){
     std::stringstream ss;
	ss << "\tli $v0, 2" << std::endl;
     ss << "\tl.s $f12, "  << bytes << "(" << s << ")" << " #PRINT" << std::endl;
     ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::sysPrintVariableBool(std::string s, int bytes){
     std::stringstream ss;
	ss << "\tli $v0, 1" << std::endl;
     ss << "\tlb $a0, "  << bytes << "(" << s << ")" << " #PRINT" << std::endl;
     ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}


void CTM::Driver::sysPrintString(std::string stringValue){
     std::stringstream ss;
	ss << "\tli $v0, 4" << std::endl;
     ss << "\tla $a0, " << stringValue << std::endl;
     ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}
 
void CTM::Driver::sysReadInteger(){
     std::stringstream ss;
	ss << "\tli $v0, 5" << std::endl;
     ss << "\tsyscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::sysReadFloat(){

     std::stringstream ss;
	ss << "li $v0, 6" << std::endl;
     ss << "syscall" << std::endl;std::fputs(ss.str().data(),outCodeSegmentFile);
}



void CTM::Driver::mtc1(std::string d, std::string s){
	 std::stringstream ss;
	ss << "\tmtc1 "<< d << ", " << s << " #Move to float register" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::mfc1(std::string s, std::string d){
	 std::stringstream ss;
	ss << "\tmfc1 "<< s << ", " << d << " #Move to int register" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::cvtIntToFloat(std::string d, std::string s){
	 std::stringstream ss;
	ss << "\tcvt.s.w "<< d << ", " << s << " #Convert int to float" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::cvtFloatToInt(std::string d, std::string s){
	 std::stringstream ss;
	ss << "\tcvt.w.s "<< d << ", " << s << " #Convert float to int" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::lwc1(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\tlwc1 "<< d << ", " << bytes << "(" << s << ")" << " #Load float register" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::swc1(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\tswc1 "<< d << ", " << bytes << "(" << s << ")" << " #Store float register" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::sw(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\tsw "<< d << ", " << bytes << "(" << s << ")" << " #Store" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::saveFloat(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\ts.s "<< d << ", " << bytes << "(" << s << ")" << " #Store" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::sb(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\tsb "<< d << ", " << bytes << "(" << s << ")" << " #Store" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::lb(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\tlb "<< d << ", " << bytes << "(" << s << ")" << " #Load" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}
void CTM::Driver::lw(std::string d, std::string s, int bytes){
	 std::stringstream ss;
	ss << "\tlw "<< d << ", " << bytes << "(" << s << ")" << " #Load" << std::endl;
	std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::startLabelNext(){
   
	declaredLocalStack[varSpace].pop_back();
     std::string label = "NEXT_";
     label += std::to_string(lNextCounter.back()); label += ":";
     lNextCounter.pop_back();
     std::stringstream ss;
     ss << label << std::endl;
     std::fputs(ss.str().data(),outCodeSegmentFile);   
}

void CTM::Driver::labelNext(){
     lNextCounter.push_back(nextCounter);

     std::string label = "bne  $t0, 1, NEXT_"; /*if false, skip to NEXT*/

     label += std::to_string(nextCounter);
     nextCounter++;
 std::stringstream ss;
     ss << label << std::endl;
std::fputs(ss.str().data(),outCodeSegmentFile);

   //nova mapa stacku
   std::map<std::string, VariableIdentifier> nova;
	
   pushLocalStack(nova);std::cout <<"novej stack"<<std::endl;
}

void CTM::Driver::startLabelEnd(){
     //vymazani stacku
     declaredLocalStack[varSpace].pop_back();

     std::string label = "END_";
     label += std::to_string(lEndCounter.back()); label += ":";
     lEndCounter.pop_back();
     std::stringstream ss;
     ss << label << std::endl;
     std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::labelEnd(bool elseif){     
     std::string label = "j END_"; /*jump to END*/

     label += std::to_string(endCounter);
     std::stringstream ss;
     ss << label << std::endl;
     std::fputs(ss.str().data(),outCodeSegmentFile);

     if(!elseif) {
       //vymazani stacku
       declaredLocalStack[varSpace].pop_back();

       std::string label = "NEXT_";
       label += std::to_string(lNextCounter.back()); label += ":";
       lNextCounter.pop_back();

       ss << label << std::endl;
       std::fputs(ss.str().data(),outCodeSegmentFile);

        //nova mapa stacku
        std::map<std::string, VariableIdentifier> nova;

        pushLocalStack(nova);std::cout <<"novej stack"<<std::endl;

     	lEndCounter.push_back(endCounter);
	endCounter++;
     }
}

void CTM::Driver::startLabelWhile() {
std::stringstream ss;
     lWhileCounter.push_back(whileCounter);
     std::string label = "WHILE_";
     label += std::to_string(whileCounter); label += ":";

     ss << label << std::endl;
     std::fputs(ss.str().data(),outCodeSegmentFile);

     whileCounter++;
}

void CTM::Driver::labelWhile() {
std::stringstream ss;
	std::string label = "j WHILE_";
    	label += std::to_string(lWhileCounter.back());

      ss << label << std::endl;
      std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::whileExpression() {
	std::stringstream ss;
	std::string label = "bne $t0, 1, END_WHILE_";
    	label += std::to_string(lWhileCounter.back());

      ss << label << std::endl;
      std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::startWhileEnd() {
	std::stringstream ss;
	std::string label = "END_WHILE_";
    	label += std::to_string(lWhileCounter.back()); label += ":";

      ss << label << std::endl;
      std::fputs(ss.str().data(),outCodeSegmentFile);

	    lWhileCounter.pop_back();
}

void CTM::Driver::whileBreak() {
	std::stringstream ss;
	std::string label = "j END_WHILE_";
    	label += std::to_string(lWhileCounter.back()); label += ":";

      ss << label << std::endl;
      std::fputs(ss.str().data(),outCodeSegmentFile);
}

void CTM::Driver::checkIfWhile() {
	if(lWhileCounter.empty()) {
     printError("Break or continue could be used only in while statement");
	}
}


