int inteager0;
int inteager1 = 1;
int inteager2 = 2;

const int inteager3;
int const inteager4 = 3;
const int inteager5 = 4;

bool boolean0;
bool boolean1 = true;

const bool boolean2;
bool const boolean3 = true;

float floatingPoint0;
float floatingPoint1 = 5.1;
float floatingPoint2 = 5.2;

float floatingPoint3;
float floatingPoint4 = 6.1;
float floatingPoint5 = 6.2;

void test_localInit();
void test_print();

void test_expression();

int main() {
	
	test_localInit();
	test_print();
	test_expression();
	
}

int a(int a) {
	return a;
}

int b(int a) {
	int b = 1;
	return a + b; // = a + 1
}

float c(float a, float b) {
	return a+b;
}

void test_expression() {
	print "TEST EXPRESSION\n";
	float d = 1.0;
	float c = 3.1415928;
	d = c(d, c);
	int b = b(6); // = 6 + 1 = 7
	float a = (float)a(5) + (float)5 + (float)b + c; // = 5.0 + 5.0 + 7.0 + 3.1415928 = 20.1415928
	
	print a; // 20.1415928
	print " = 20.1415928\n";
	print b; // 7
	print " = 7\n";
	print c; // 3.1415928
	print " = 3.1415928\n";
	print d; // 4.2415928
	print " = 4.2415928\n";
}


void test_print() {
	print "TEST PRINT\nvalue\t\t|\ttext\n";

	print 1;
	print "\t\t|\t1\n";

	print 2.0;
	print "\t|\t2.1\n";

	print false;
	print "\t\t|\t0\n";
	
	int a = 3;
	print a;
	print "\t\t|\t3\n";

	float b = 4.2;
	print b;
	print "\t|\t4.2\n";

	bool c = true;
	print c;
	print "\t\t|\t1\n";

	const int d = 5;
	print d;
	print "\t\t|\t5\n";

	const float e = 6.3;
	print e;
	print "\t|\t6.3\n";

	const bool f = false;
	print f;
	print "\t\t|\t0\n";
	
	
}


void test_localInit() {
	int inteager0;
	int inteager1 = 12;
	int inteager2 = 13;

	const int inteager3;
	int const inteager4 =14;
	const int inteager5 = 15;

	bool boolean0;
	bool boolean1 = false;

	const bool boolean2;
	bool const boolean3 = false;

	float floatingPoint0;
	float floatingPoint1 = 16.1;
	float floatingPoint2 = 16.2;

	const float floatingPoint3;
	float const floatingPoint4 = 17.1;
	const float floatingPoint5 = 17.2;

	print "OK - test_init() successful\n";
}


