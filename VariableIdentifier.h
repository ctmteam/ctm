#ifndef __VARIABLE_IDENTIFIER_H__
#define __VARIABLE_IDENTIFIER_H__

#include "EType.h"
#include "EExp.h"
#include <string>
#include <vector>

#include <iostream>

namespace CTM{
/**
 * Structure for storing variables
 **/
class VariableIdentifier {
private:
	std::string _identifier; /* Variable's name */
	EType _type; /* Variable's type */
	bool _isConstant; /* True, if is constant, false otherwise */
	
	std::vector<std::pair<std::string, CTM::EExp>> valueStack; /* stack for values */
	int _sizeArray; /* size of array */
	int _positionStack; /* position in stack */
	bool _haveData; /* if has data or not */
	bool _isGlobal; /* if is global or not */

public:
	VariableIdentifier();
	explicit VariableIdentifier(std::string, EType, bool, int positionStack, bool isGlobal);

	VariableIdentifier(const CTM::VariableIdentifier&);
	
	std::string identifier();
	std::string identifier() const;

  /**
   * Get type
   * @return type
  **/
	EType type();

  /**
   * Get array size
   * @return array size
  **/
	int sizeArray();

  /**
   * Set array size
   * @param size - array size
   **/
	void setSizeArray(int size);

  /**
   * If is constant or not
   * @return true if is const
  **/
	bool isConstant();

  /**
   * If has data or not
   * @return true if has data
  **/
	bool haveData();

  /**
   * Set if has data
   * @param have - true if has data, false otherwise
   **/
	void setHaveData(bool have);

  /**
   * If is global or not
   * @return true if is global
  **/
  bool isGlobal();

  /**
   * Set is global
   * @param global - true if global, false otherwise
   **/
	bool setIsGlobal(bool global);
	
  /**
   * Get position in stack
   * @return position
  **/
	int positionStack();

   /**
   * Set position in stack
   * @param positionStack - position
   **/
	void setPositionStack(int positionStack);

   /**
   * Push value into stack
   * @param - value
   **/
	void pushStackValues(std::pair<std::string, CTM::EExp>);

  /**
   * Get size of stack
   * @return stack size
  **/
	int getStackSize();

    /**
   * Pop value from stack
   * @param index - value index
   **/
	std::pair<std::string, CTM::EExp> popStackValues(int index);	
	std::pair<std::string, CTM::EExp> popBackStackValues();	

   /**
   * Set ientifier
   * @param - identifier
   **/
	void setIdentifier(std::string);

  /**
   * Clear stack with values
   **/
	void clearValueStack();	
	 
   /**
    * Creates VariableIdentifier
    * @param - name of variable
    * @param - type of variable
    * @param - position in stack
    * @param - if is global or not
    **/
	static VariableIdentifier fromVectorFactory(std::string &, std::vector<EType> &, int, bool); 

	VariableIdentifier& operator=(const VariableIdentifier&);
	VariableIdentifier& operator=(VariableIdentifier&&);

	int getSize();
};

} /* end namespace CTM */
#endif /* END __VARIABLE_IDENTIFIER_H__ */
