#include "ADriver.h"

CTM::ADriver::~ADriver() {

}

void CTM::ADriver::parseStream( std::istream &stream ) {
   try {
      scanner.reset(new CTM::Scanner(&stream));
   }
   catch( std::bad_alloc &ba )
   {
      std::cerr << "Failed to allocate scanner: (" <<
         ba.what() << "), exiting!!\n";
      exit( EXIT_FAILURE );
   }
   
   try {
      parser.reset(new CTM::Parser(*scanner, *this));
   }
   catch( std::bad_alloc &ba )
   {
      std::cerr << "Failed to allocate parser: (" << 
         ba.what() << "), exiting!!\n";
      exit( EXIT_FAILURE );
   }

   outputFile.open("./output.asm");
   outDataSegmentFile = std::tmpfile();
   outCodeSegmentFile = std::tmpfile();
   outDataStringSegmentFile = std::tmpfile();
   printFileHeader();

   if( parser->parse() != 0 )
   {
      std::cerr << RED_COLOR << "Parse failed!!" << DEFAULT_COLOR << std::endl;
   }
   else {
   	std::cout << GREEN_COLOR << "File parsed succesfuly" << DEFAULT_COLOR << std::endl;
   }
   
   saveOutPutFile();

   fclose(outDataSegmentFile);
   fclose(outCodeSegmentFile);
   fclose(outDataStringSegmentFile);
   outputFile.close();
}


