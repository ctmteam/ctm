#include "EType.h"

namespace CTM {

std::string toString(EType &type) {
	switch(type) {
		case EType::VOID:
			return "void";
		case EType::BOOL:
			return "bool";
		case EType::INT:
			return "int";
		case EType::FLOAT:
			return "float";
		case EType::CONST:
			return "const";
		default:
			return "";
	}
}

}
