#ifndef __FUNCTION_IDENTIFIER_H__
#define __FUNCTION_IDENTIFIER_H__

#include "EType.h"
#include "VariableIdentifier.h"

#include <string>
#include <sstream>
#include <vector>

namespace CTM{
/**
 * Structure for storing functions
 **/
class FunctionIdentifier {
private:
   std::string _name; /* Function's name */
   std::string _ordinaryName; /*  */
   std::vector<VariableIdentifier> _parameters; /* Function's parameters */
   EType _returnType; /* Return type of function */
   bool _defined; /* If is defined or not */

public:
   FunctionIdentifier();
   explicit FunctionIdentifier(EType, std::string &, std::vector<VariableIdentifier> &, bool);
   FunctionIdentifier& operator=(const FunctionIdentifier&);

   std::string name();
   std::string name() const;

   std::string ordinaryName();
   std::string ordinaryName() const;

   bool defined();
   bool defined() const;

   /**
    * Set defined = true
    **/
   void setDefined();

   /**
    * Get function's parameters
    * @return vector with function's parameters
    **/
   std::vector<VariableIdentifier> &parameters();

   /**
    * Get function's return type
    * @return function's return type
    **/
   EType returnType();


   static std::string getOrdinaryName(FunctionIdentifier &);
   static std::string getOrdinaryName(std::string &, std::vector<EType> &);
};

} /* end namespace CTM */
#endif /* END __FUNCTION_IDENTIFIER_H__ */
