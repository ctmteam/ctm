#ifndef __E_EXP_H__
#define __E_EXP_H__

#include "EType.h"

#include <string>

namespace CTM{
/**
* Enum for expression's enabled types
**/
enum class EExp  {
	VAR,
	BOOL,
	INT,
	VOID,
	FUNC,
	FLOAT
	
};

std::string toString(EExp &);
int getSize(EExp &);
EExp convert(EType &);
}

#endif /* END __E_Exp_H__ */

