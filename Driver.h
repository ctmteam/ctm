#ifndef __DRIVER_H__
#define __DRIVER_H__ 

#include "ADriver.h"
#include "Scanner.h"
#include "parser.tab.hpp"

#include "EType.h"
#include "EExp.h"
#include "VariableIdentifier.h"
#include "FunctionIdentifier.h"
#include <iostream>
#include <iomanip>
#include <map>
#include <memory>

namespace CTM{

class Driver : public ADriver {
public:
   Driver();
   ~Driver();
   
   /** 
    * parsing input file
    * @param filename - valid path to input file
    */
   void parse( const char * const filename );

    /**
    * pushing identifier into stack with identifiers
    * @param - identifier
    */
   void pushIdentifier(std::string);

    /**
    * pushing type into stack with types
    * @param - type
    */
   void pushType(EType);

    /**
    *
    */
   void createVariableIdentifer();
   
    /**
    * set flag about function declaration
    * @param - true if is
    */
   void setIsFunctionDeclaration(bool);

    /**
    * Push constant info stack
    * @param - name and type of constant
    */
   void setConstant(std::pair<std::string, CTM::EExp>);
   
    /**
    * Checks if function is defined
    * @param FunctionIdentifier - function's identifier
    * @return true if is defined, 0 otherwise
    */
   bool haveDefinedFunctionIdentifier(const FunctionIdentifier &);

    /**
    * Push function identifier into map
    * @param FunctionIdentifier - function's identifier
    */
   void pushFunctionIdentifier( const FunctionIdentifier &);

    /**
    * Push variable identifier into map
    * @param name - variable's name
    * @param types - type of variable
    * @param haveValue - true if is inicialized
    */
   void pushVariableIdentifier(std::string &name, std::vector<EType> &types, bool haveValue);
   virtual void pushVariableIdentifier(std::string &name, std::vector<EType> &types, bool haveValue, bool);

   void localVariableDeclared(VariableIdentifier &, bool haveValue);


 	/**
    * Cast to another type
    * @param type - result's type
    * @param operator2 - casting value
    **/
   std::pair<std::string, EExp> castOp(CTM::EType type, std::pair<std::string, CTM::EExp> operator2);
   std::pair<std::string, EExp> addOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
  
   std::pair<std::string, EExp> subOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> mulOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> divOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> andOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> orOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> eqOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> nonEqOp(std::pair<std::string, CTM::EExp> value1, std::pair<std::string, CTM::EExp> value2);
   std::pair<std::string, EExp> exclamationOP(std::pair<std::string, CTM::EExp> value);



   void printGlobalIntIdentifer(VariableIdentifier &, bool haveValue);
   void printGlobalFloatIdentifer(VariableIdentifier &, bool haveValue);
   void printGlobalBoolIdentifer(VariableIdentifier &, bool haveValue);

   void printGlobalIntArrayIdentifer(VariableIdentifier &variable, bool haveValue);
   void printGlobalFloatArrayIdentifer(VariableIdentifier &variable, bool haveValue);
   void printGlobalBoolArrayIdentifer(VariableIdentifier &variable, bool haveValue);
   void assigmentExppresion();
 
   void printValue(std::pair<std::string, CTM::EExp> value);
   void printString(std::string value);

    /**
    * Set size of last array
    * @param - array size
    **/
   void setLastArraySize(std::string);
 
    /**
    * Set last indent
    * @param lastIdent - last ident
    **/
	 void setLastIdent(std::string lastIdent);

    /**
    * Set last type
    * @param lastType - last type
    **/
	 void setLastType(CTM::EType lastType);

    /**
    * Set last index
    * @param index - last index
    **/
	 void setLastIndex(std::string index);

    /**
    * On return jump out of function
    **/
   virtual void jumpOutOfFunction();

    /**
    * On return jump out of function
    * @param - return statement
    **/
   virtual void jumpOutOfFunction(std::pair<std::string, EExp> &);

    /**
    * Jump into function by name parameter
    * @param - function name
    **/
   virtual void callFunction(std::string &);

    /**
    * Jump into function by name parameter
    * @param - function name
    * @param - function parameters
    **/
   virtual void callFunction(std::string &, std::vector<std::pair<std::string, CTM::EExp>> &);

   /* Setting labels for if-else if-else */
	 void labelEnd(bool elseif);
	 void startLabelEnd();
	 void startLabelNext();
	 void labelNext();

   /* Setting labels for while loop */
   void startLabelWhile();
   void labelWhile();
   void whileExpression();
   void startWhileEnd();
   void whileBreak();
   void checkIfWhile();

protected:
   /* Prints header for .asm file */
   void printFileHeader();
   /* Saves output .asm file */
   void saveOutPutFile();
   
private:
   std::string varSpace;

    /**
    * Create error message from string parts
    * @param msg1 - first string
    * @param msg2 - second string
    * @param msg3 - third string
    * @param msg4 - fourth string
    * @return - error message
    **/
   std::string createErrMsg(std::string msg1, std::string msg2, std::string msg3, std::string msg4);

   /**
    * Prints error message and exit transtalion
    * @param msg - Error message
    **/
   void printError(std::string msg);
   
   /** map for store global variables */
   std::map<std::string, VariableIdentifier> declaredGlobalVariables;
   /** map for store maps with local variables */
   std::map<std::string, std::vector<std::map<std::string, VariableIdentifier>>> declaredLocalStack;
   /** map for store declared functions */
   std::map<std::string, std::vector<FunctionIdentifier>>  declaredFunctions;
 
   std::vector<std::string> outPutDataStack;
   std::vector<std::string> outPutHeaderStack;
  
   /** stack for identifiers */
   std::stack<std::string> identifierStack;
   /** stack for values */
   std::vector<std::pair<std::string, CTM::EExp>> valueStack;
   /** stack for types */
   std::stack<EType> typeStack;

   void localArrayDeclared(VariableIdentifier &variable, bool haveValue);
   void initBoolToReg(std::string reg,std::string value);
   int lastArraySize; /* size of last array */
   int lastLocalSP;  /* last stack in map of local variables */
   int cntPrintString;
   std::stack<EType> lastTypeStack; /* last type from stack */
   std::stack<int> lastIndex; /* last index of stack */

   std::string actualFunction; /* Actual function */
   bool isFunctionDeclaration; /* If function is declared or not */
   int stackPointer; /* pointer to stack */
   int globalPointer; /* pointer for global variables */
   int lastFindIndexStack; /* Index of last found stack */
	
   bool floatIsContained; /* if is fleat contained or not */

   std::string lastIdent; /* last indent */
   CTM::EType lastType; /* Last used type */
   CTM::EType lastPartResult; /* last part result of expression */
   
   /**
    * Load content into register
    * @param reg - destination register
    * @param type - content type
    **/
   void getFromStack(std::string reg, CTM::EType type);

    /**
    * Set content into stack
    * @param reg - destination register
    * @param type - content type
    **/
   void setToStack(std::string reg, CTM::EType type);

   /**
    * Determine part result of expression
    * @param value - first value
    * @param reg - destination register
    * @param type - type of first value
    * @param otherValue - second value
    * @param otherType - type of second value
    **/
   std::string deterPartResult(std::string value, std::string reg, CTM::EExp type,  std::string otherValue, CTM::EExp otherType);

   void moveToStack(int index);

   /**
    * Pushing new map of local variables into stack
    * @param - new map
    **/
   void pushLocalStack(std::map<std::string, VariableIdentifier>&);	

   /**
    * Finding variable in local and global variable map
    * @param var - name if search variable
    **/
   virtual VariableIdentifier findVariable(std::string var);


   virtual FunctionIdentifier findFunctionByOrdinary(std::string &);

   /**
    * Saves local variable
    * @param variable - variable identifier
    * @param lastStackValue - last pushed value into stack
    **/

   void saveLocalValue(VariableIdentifier &variable,std::pair<std::string, CTM::EExp> lastStackValue);

   /* Vector stores if - else if - else numbers */
   std::vector<int> lNextCounter;
   std::vector<int> lEndCounter;

   /* Vector stores while's numbers */
   std::vector<int> lWhileCounter;

   /* Counters for if - else if - else */
   int nextCounter;
   int endCounter;

   /* Counter for while */
   int whileCounter;
   int expressionJumpCounter;

   EExp resolveType(EExp &, std::string &);
   std::string convertBoolValue(std::string &);
   virtual void setVarSpace(std::string &);
   virtual void clearVarSpace();

   /* Integer arithmetic MIPS instruction */
   /**
    * Addition two integers
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void addInt(std::string d,std::string s,std::string t);

   /**
    * Addition two integers
    * @param t - register for result
    * @param s - register with first value
    * @param C - int as second value
    */
   void addiInt(std::string t,std::string s,int C); 

    /**
    * Subtraction two integers
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void subInt(std::string d,std::string s,std::string t);


    /**
    * Multiplication two integers
    * @param s - register with first value
    * @param t - register with second value
    */
   void multInt(std::string d, std::string s,std::string t);

    /**
    * Division two integers, result is stored special register
    * @param s - register with first value
    * @param t - register with second value
    */
   void divInt(std::string s,std::string t);
  
   /* Float arithmetic MIPS instruction */
    /**
    * Addition two floats
    * @param x - register for result
    * @param y - register with first value
    * @param z - register with second value
    */
   void addFloat(std::string x,std::string y,std::string z);

    /**
    * Subtraction two floats
    * @param x - register for result
    * @param y - register with first value
    * @param z - register with second value
    */
   void subFloat(std::string x,std::string y,std::string z);

    /**
    * Multiplication two floats
    * @param x - register for result
    * @param y - register with first value
    * @param z - register with second value
    */
   void mulFloat(std::string x,std::string y,std::string z);

    /**
    * Division two floats
    * @param x - register for result
    * @param y - register with first value
    * @param z - register with second value
    */
   void divFloat(std::string x,std::string y,std::string z);
   
   /* Logical MIPS instruction */
    /**
    * Logical and between two registers
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void andL(std::string d,std::string s,std::string t);

    /**
    * Logical and between register and int
    * @param t - register for result
    * @param s - register with first value
    * @param C - integer value
    */
   void andiL(std::string t,std::string s,int C);

    /**
    * Logical or between two registers
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void orL(std::string d,std::string s,std::string t);

    /**
    * Logical or between register and int
    * @param t - register for result
    * @param s - register with first value
    * @param C - integer value
    */
   void oriL(std::string t,std::string s,int C);

    /**
    * Logical xor between two registers
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void xorL(std::string d,std::string s,std::string t);

    /**
    * Logical or between register and int
    * @param t - register for result
    * @param s - register with first value
    * @param C - integer value
    */
   void xoriL(std::string t,std::string s,int C);

    /**
    * Logical nor between two registers
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void nor(std::string d,std::string s,std::string t);

    /**
    * If $s is less than $t, $d is set to 1. 0 otherwise.
    * @param d - register for result
    * @param s - register with first value
    * @param t - register with second value
    */
   void slt(std::string d,std::string s,std::string t);

    /**
    * If $s is less than C, $t is set to 1. 0 otherwise.
    * @param t - register for result
    * @param s - register with first value
    * @param C - integer value
    */
   void slti(std::string t,std::string s,int C);
   
   /* Bitwise Shift MIPS instruction */
    /**
    * Shifts t value left by the shift amount and places the result in d.
    * @param d - register for result
    * @param t - processed register
    * @param shamt - shift amount
    */
   void sll(std::string d,std::string t,int shamt);

    /**
    * Shifts t right by the shift amount and places the value in d.
    * @param d - register for result
    * @param t - processed register
    * @param shamt - shift amount
    */
   void srl(std::string d,std::string t,int shamt);

    /**
    * Shifts t value left by the value in s and places the result in d.
    * @param d - register for result
    * @param t - processed register
    * @param s - shift ammount
    */
   void sllv(std::string d,std::string t,std::string s);

    /**
    * Shifts t value right by the amount specified in $s and places the value in d.
    * @param d - register for result
    * @param t - processed register
    * @param s - shift ammount
    */
   void srlv(std::string d,std::string t,std::string s);
  
   /* Data Transfer MIPS instruction */
   /**
    * Moves content of register HI to d.
    * @param d - destination register
    */
   void moveHi(std::string d);

    /**
    * Moves content of register Lo to d.
    * @param d - destination register
    */
   void moveLo(std::string d);

    /**
    * Loads content of register s to r.
    * @param r - destination register
    * @param s - source register
    */
   void li(std::string r,std::string s);

    /**
    * Loads float content of register s to r.
    * @param r - destination register
    * @param s - source register
    */
   void liFloat(std::string r,std::string s);

   /* Pseudo MIPS instruction */
   /**
    * Moves content of register s to r.
    * @param r - destination register
    * @param s - source register
    */
   void move(std::string r,std::string s);
   void clear(std::string rt);
   void notP(std::string rt,std::string rs);
  
   /* Branch MIPS instruction */
    /**
    * Branches if registers are equal.
    * @param s - first register
    * @param t - second register
    * @param loop -
    */
   void beq(std::string s,std::string t,std::string loop);
   void bne(std::string s,std::string t,std::string loop);
   void bc1t(std::string loop);
   void bc1f(std::string loop);
   void c_eq_s(std::string s, std::string d);

   /* Jump MIPS instruction */
   void jr(std::string s);
   virtual void jal(std::string &);

   /* Syscal MIPS instruction */
   void sysPrintInt(int intValue);
   void sysPrintFloat(std::string &floatValue);
   void sysPrintString(std::string stringValue);
 
   void sysPrintVariableInt(std::string s, int bytes);
   void sysPrintVariableFloat(std::string s, int bytes);
   void sysPrintVariableBool(std::string s, int bytes);
   
   void sysReadInteger();
   void sysReadFloat();

   void mtc1(std::string d, std::string s);
   void mfc1(std::string s, std::string d);

   void cvtIntToFloat(std::string d, std::string s);
   void cvtFloatToInt(std::string d, std::string s);

   void lwc1(std::string d, std::string s, int bytes);
   void swc1(std::string d, std::string s, int bytes);
   void sw(std::string d, std::string s, int bytes);
   void saveFloat(std::string d, std::string s, int bytes);
   void sb(std::string d, std::string s, int bytes);
   void lb(std::string d, std::string s, int bytes);


    /**
    * Loads word into a register from the specified address.
    * @param d - destination register
    * @param s - address in memory
    * @param bytes - offset
    */
   void lw(std::string d, std::string s, int bytes);
};

} /* end namespace CTM */
#endif /* END __DRIVER_H__ */
